import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  TextInput,
  FlatList,
  Button
} from 'react-native';

import io from 'socket.io-client';
import constant from './src/common/constant';
import action from './src/common/action';
import helper from './src/common/helper';
import * as Http from './src/utils/http';

export default class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      socket: null,
      message: '',
      userId: '',
      data: [
        { id: 1, date: "9:50 am", is_receive_msg: true, sender_id: 'a8ff530f-c32d-4c33-9c76-4ad9c874ce541575397990113', receiver_id: '5aa84377-c0d1-48f7-9d2a-cef943662f441575397996572', content: "Xin chao, khoe khong?" },
        { id: 2, date: "9:50 am", is_receive_msg: false, sender_id: '5aa84377-c0d1-48f7-9d2a-cef943662f441575397996572', sendreceiver_ider_id: 'a8ff530f-c32d-4c33-9c76-4ad9c874ce541575397990113', content: "Khoe :))" },
      ]
    };
    this.handleSendMessage = this.handleSendMessage.bind(this);
  }

  componentDidMount() {
    const socket = io(`${constant.URL.SOCKET}`);

    Http.login('demo2', 'Abc123456')
      .then(userInfo => {
        this.setState({
          socket,
          userId: userInfo.user_id
        }, () => {
          this.state.socket.emit(action.RESPONSE.USER_INFO, {
            user_id: userInfo.user_id,
            username: 'demo2'
          });
          this.state.socket.emit(action.REQUEST.CHAT_TO_SOMEONE, { sender_id: this.state.userId, receiver_id: constant.demo1 });

          this.state.socket.on(action.RESPONSE.SEND_MESSAGE, messageInfo => {
            let newMessage = messageInfo;
            if (newMessage) {
              if (newMessage.sender_id === this.state.userId) {
                newMessage.id = this.state.data.length + 1;
                newMessage.date = helper.convertTimeDisplay(new Date().getTime())
                newMessage.is_receive_msg = false;
              } else {
                newMessage.id = this.state.data.length + 1;
                newMessage.date = helper.convertTimeDisplay(new Date().getTime())
                newMessage.is_receive_msg = true;
              }
              this.setState({
                data: [...this.state.data, newMessage]
              });
            }
          })
        });
      })
      .catch(err => {
        alert('Unknow Error!');
        console.log(err);
      });
  }

  handleSendMessage() {
    const { socket, message } = this.state;
    if (!socket) return;
    if (this.state.userId !== '') {
      socket.emit(action.REQUEST.SEND_MESSAGE, {
        sender_id: this.state.userId,
        receiver_id: constant.demo1,
        content: message
      });
      this.setState({
        message: ''
      })
    } else {
      alert('Send message fail');
    }
  }

  renderDate = (date) => {
    return (
      <Text style={styles.time}>
        {date}
      </Text>
    );
  }

  render() {

    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          data={this.state.data}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={(message) => {
            console.log(item);
            const item = message.item;
            let inMessage = item.is_receive_msg === true;
            let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
            return (
              <View style={[styles.item, itemStyle]}>
                {!inMessage && this.renderDate(item.date)}
                <View style={[styles.balloon]}>
                  <Text>{item.content}</Text>
                </View>
                {inMessage && this.renderDate(item.date)}
              </View>
            )
          }} />
        <View style={styles.footer}>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
              placeholder="Write a message..."
              underlineColorAndroid='transparent'
              value={this.state.message}
              onChangeText={(value) => this.setState({ message: value })}
            />
          </View>

          <TouchableOpacity style={styles.btnSend} onPress={this.handleSendMessage}>
            <Image source={{ uri: "https://png.icons8.com/small/75/ffffff/filled-sent.png" }} style={styles.iconSend} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  list: {
    paddingHorizontal: 17,
  },
  footer: {
    flexDirection: 'row',
    height: 60,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 10,
    padding: 5,
  },
  btnSend: {
    backgroundColor: "#00BFFF",
    width: 40,
    height: 40,
    borderRadius: 360,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconSend: {
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,
  },
  inputs: {
    height: 40,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  balloon: {
    maxWidth: 250,
    padding: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start'
  },
  itemOut: {
    alignSelf: 'flex-end'
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
    fontSize: 12,
    color: "#808080",
  },
  item: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: "#eeeeee",
    borderRadius: 300,
    padding: 5,
  },
});  