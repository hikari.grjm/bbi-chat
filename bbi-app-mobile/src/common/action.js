module.exports = {
    COMMON: {
        CONNECTION: 'connection',
        CONNECTED: 'connected',
        DISCONNECT: 'disconnect',
        ERROR: 'error'
    },
    REQUEST: {
        USER_INFO: 'req#user_info',
        JOIN_ROOM: 'req#join_room',
        CHAT_TO_SOMEONE: 'req#chat_to_someone',
        SEND_MESSAGE: 'req#send_message'
    },
    RESPONSE: {
        USER_INFO: 'res#user_info',
        LIST_USERS_ONLINE: 'res#list_users_online',
        JOIN_ROOM: 'res#join_room',
        CHAT_TO_SOMEONE: 'res#chat_to_someone',
        SEND_MESSAGE: 'res#send_message'
    }
};