const baseUrlAPI = 'http://chat.bbivietnam.vn';

module.exports = {
    URL: {
        SOCKET: 'http://localhost:7000',

        login: `${baseUrlAPI}/api/auth`,
        relation: `${baseUrlAPI}/api/relation`,
        refresh: `${baseUrlAPI}/api/refresh`
    },
    demo1: "a8ff530f-c32d-4c33-9c76-4ad9c874ce541575397990113"
};