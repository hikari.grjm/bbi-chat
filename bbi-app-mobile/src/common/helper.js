module.exports = {
    convertTimeDisplay: (time) => {
        try {
            if (time != null && new RegExp('^[0-9]+$').test(time))
                return new Date(time).toLocaleTimeString();
            return time;
        } catch (error) {
            return time;
        }
    },
    replaceKeyDic: valueInput => {
        if (!valueInput) return '';
        return valueInput.replace(/-/g, '_');
    },
    isObjectEmpty: obj => {
        for (const key in obj) {
            if (Reflect.has(obj, key)) {
                return false;
            }
        }
        return true;
    }
}