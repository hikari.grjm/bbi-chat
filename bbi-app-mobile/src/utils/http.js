import Axios from 'axios'
import { URL } from '../common/constant';

let RoleGroup = null
let token = null
let userInfo = null


export function uploadDataFile(file, param, onUploadProgress) {
    let formData = new FormData();
    formData.append('Files', file);
    formData.append('Param', param);
    return post({
        url: URL.file,
        data: formData,
        contentType: 'multipart/form-data',
        onUploadProgress
    }).then(res => {
        return res
    })
}

export function login(username, password) {
    return post({
        url: URL.login,
        data: {
            username: username,
            password: password
        },
        nonToken: true
    }).then(res => {
        if (!res || !res.token || !res.userInfo) return null;
        saveUserInfo(res)
        return res.userInfo;
    })
}

export function queryRoleGroup() {
    return get({
        url: URL.roleList
    }).then(res => {
        if (!res || !res.Success || !res.Data) return false
        RoleGroup = [...res.Data]
        return true
    })
}

export function getRelationship() {
    return get({
        url: URL.relation
    }).then(res => {
        if (!res) return [];
        return res;
    })
}

export function getRoleGroup() {
    return [...RoleGroup]
}

export function deleteUser(username) {
    return del({
        url: URL.deleteUser + '/' + username
    }).then(res => {
        return res
    })
}

export function createUser(userInfo) {
    return post({
        url: URL.createUser,
        data: userInfo
    }).then(res => {
        return res
    })
}

export function updateUser(userInfo) {
    return put({
        url: URL.updateUser,
        data: userInfo
    }).then(res => {
        return res
    })
}

export function getCurrentUserInfo() {
    return { ...userInfo }
}

function saveUserInfo(resLogin) {
    token = resLogin.token;
    renewAccessToken(8)
}

function renewAccessToken(time) {
    const user = getCurrentUserInfo()
    setTimeout(() => {
        post({
            url: URL.refresh,
            data: {
                username: user.username
            }
        }).then(res => {
            if (!res) {
                renewAccessToken(time)
            } else {
                token = res.token;
                renewAccessToken(time)
            }
        })
    }, time * 60 * 1000 * 0.75);
}

function get({ url, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.get(
            url, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Get ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function post({ url, data, nonToken = false, contentType, onUploadProgress }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }
        if (contentType) {
            if (!config.headers) config.headers = {}
            config.headers['Content-Type'] = contentType
        }
        if (onUploadProgress) {
            config.onUploadProgress = onUploadProgress
        }

        Axios.post(
            url, data, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Post ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function put({ url, data, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.put(
            url, data, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Put ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function del({ url, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.delete(
            url, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Delete ${url} fail: ${error}`)
            resolve(null)
        });
    })
}