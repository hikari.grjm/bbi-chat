const io = require('socket.io-client');
const constant = require('../src/common/constant');
const action = require('../src/common/action');
const socket = io(`${constant.URL.SOCKET}`);
const utils = require('./utils');

if (socket) {
    socket.on('connect', () => {
        console.log('ON SOCKET_1' + socket.id);
        utils(socket).emitJoinRoom([1, 2, 231], () => { });
    })

    utils(socket).onJoinRoom(() => {
        console.log('JOIN ROOM SUCCESS');
        utils(socket).emitUserInfo(1, 'AK_1', () => {
            console.log('SEND USER INFO SUCCESS');
        });
    })

    utils(socket).emitUserStatus(1, 0, () => {
        console.log('EMIT USER_STATUS TO 0');
    });


    socket.on(action.RESPONSE.CHANNEL.USER_STATUS, data => {
        console.log('RESPONSE CHANNEL USER_STATUS' + JSON.stringify(data));
    });
    socket.on(action.RESPONSE.ALL.USER_STATUS, data => {
        console.log('RESPONSE ALL USER_STATUS' + JSON.stringify(data));
    });

    socket.emit(action.REQUEST.SERVER.LIST_USER_STATUS, [10160, 446934, 526144, 390875]);

    socket.on(action.RESPONSE.SPECIFIC.LIST_USER_STATUS, data => {
        console.log('LIST_USER_STATUS' + JSON.stringify(data));
    });

    // utils(socket).emitChatToSomeOne(123, 2, () => {
    //     utils(socket).emitSendMessage(123, 1, 'Xin chao', 'text', () => { });
    // })

    // utils(socket).onSendMessage(msg => {
    //     console.log(`SOCKET_1: ${JSON.stringify(msg)}`);
    // })


    // socket.on(action.ERROR.SPECIFIC.MESSAGE_STATUS, error => {
    //     console.log(error);
    // })

    // socket.on("ping", function (data) {
    //     console.log("received ping");
    // });
    // socket.on("pong", function (data) {
    //     console.log("received pong" + data);
    // });

    // socket.on("reconnecting", function (data) {
    //     console.log("reconnecting" + data);
    //     // Qua 3 lan -> ban message
    //     if (data === 3) {
    //         // socket.disconnect();
    //         socket.connect();
    //     }
    // });

    // socket.on("reconnect_failed", function (data) {
    //     console.log("received pong" + data);
    // });

    // socket.on('connect', function (data) { console.log(`connected _ ${ JSON.stringify(data) }`) });
    // socket.on('disconnect', function (data) { console.log(`disconnected _ ${ JSON.stringify(data) }`) });

    // setTimeout(() => {
    //     socket.disconnect();
    // }, 5000)
}