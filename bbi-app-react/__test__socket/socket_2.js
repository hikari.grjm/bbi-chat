const io = require('socket.io-client');
const constant = require('../src/common/constant');
const action = require('../src/common/action');
const socket = io(`${constant.URL.SOCKET}`);
const utils = require('./utils');

if (socket) {
    console.log('ON SOCKET_2');

    socket.on('connect', () => {
        console.log('connect')
        utils(socket).emitJoinRoom([1, 2, 323], () => { });
    })

    utils(socket).onJoinRoom(() => {
        console.log('JOIN ROOM SUCCESS');
        utils(socket).emitUserInfo(2, 'AK_2', () => {
            console.log('SEND USER INFO SUCCESS');
        });
    })

    socket.on(action.RESPONSE.CHANNEL.USER_STATUS, data => {
        console.log('RESPONSE CHANNEL USER_STATUS' + data);
    });

    utils(socket).emitUserStatus(2, 0, () => {
        socket.on(action.RESPONSE.CHANNEL.USER_STATUS, data => {
            console.log('aaa' + data);
        });
    });


    // socket.on("ping", function (data) {
    //     console.log("received ping");
    // });
    // socket.on("pong", function (data) {
    //     console.log("received pong" + data);
    // });

    // socket.on("reconnecting", function (data) {
    //     console.log("reconnecting" + data);
    //     // Qua 3 lan -> ban message
    //     if (data === 3) {
    //         // socket.disconnect();
    //         socket.connect();
    //     }
    // });

    // socket.on("reconnect_failed", function (data) {
    //     console.log("received pong" + data);
    // });

    // socket.on('connect', function (data) { console.log(`connected _ ${JSON.stringify(data)}`) });
    // socket.on('disconnect', function (data) { console.log(`disconnected _ ${JSON.stringify(data)}`) });

    // setTimeout(() => {
    //     socket.disconnect();
    // }, 5000)
}