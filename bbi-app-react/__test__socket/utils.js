const action = require('../src/common/action');

module.exports = (socket) => {
    return {
        emitUserInfo: (userId, userName, cb) => {
            socket.emit(action.REQUEST.SERVER.USER_INFO, {
                user_id: userId,
                username: userName
            });
            return cb();
        },
        emitSendMessage: (channelId, senderId, content, contentType, cb) => {
            socket.emit(action.REQUEST.CHANNEL.SEND_MESSAGE, {
                channel_id: channelId,
                sender_id: senderId,
                content,
                content_type: contentType
            });
            cb();
        },
        emitChatToSomeOne: (channelId, objectId, cb) => {
            socket.emit(action.REQUEST.SERVER.CHAT_TO_SOMEONE, {
                channel_id: channelId,
                object_id: objectId
            });
            cb();
        },
        emitUserStatus: (userId, status, cb) => {
            socket.emit(action.REQUEST.SERVER.USER_STATUS, {
                user_id: userId,
                status
            });
            cb();
        },
        emitJoinRoom: (listChannelId, cb) => {
            socket.emit(action.REQUEST.SERVER.JOIN_ROOM, listChannelId);
            cb();
        },
        onSendMessage: (cb) => {
            socket.on(action.RESPONSE.CHANNEL.SEND_MESSAGE, data => {
                cb(data);
            });
        },
        onJoinRoom: (cb) => {
            socket.on(action.RESPONSE.SPECIFIC.JOIN_ROOM, () => {
                cb();
            });
        }
    }
}