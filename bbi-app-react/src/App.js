import React from 'react';
import io from 'socket.io-client';
import helper from './common/helper';
import action from './common/action';
import globalz from './common/global';

import * as Http from './utils/http';

import './css/bootstrap.min.css';
import './App.css';

import constant from './common/constant';
import fakeData from './common/fake-data';

// Component
import Login from './components/Login';
import User from './components/User';
import ChatBox from './components/ChatBox';
import Search from './components/Search';
import Menu from './components/Menu';

// Core
import firbaseApp from "./core/firebase-config";

// Child
import ChattingUser from './components/Childs/ChattingUser';

const dicUsersOnline = {
  'anh_khong': {
    user_id: 'anh.khong',
    username: 'Anh Khong',
    avatar: 'https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg',
    is_online: true
  },
  'keitiey': {
    user_id: 'keitiey',
    username: 'Keitiey',
    avatar: 'https://static.turbosquid.com/Preview/2016/04/24__17_00_35/bSM_render_101.png7c53a114-0e6a-40c6-bbe5-22ee4578f48dDefaultHQ.jpg',
    is_online: true
  }
};

let firebaseMessaging = null;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      socket: null,
      message: '',
      userId: '',
      chanelId: '',
      chattingUser: {},
      isNewMessage: false,
      newMessage: null,
      isLoginSuccess: false,
      lstUsersOnline: [],
      lstMessage: [],
      isUpdateComponent: false,
      fileInput: ''
    };
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleChatTo = this.handleChatTo.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
  }
  componentDidMount() {
    const socket = io(`${constant.URL.SOCKET}`);
    this.setState({ socket });
    console.log('VAO APP');
    firebaseMessaging = firbaseApp.messaging();

    firebaseMessaging.onMessage(payload => {
      console.log(payload);
    });

    firebaseMessaging.requestPermission()
      .then(function () {
        console.log('Notification permission granted.');

        firebaseMessaging.onMessage(payload => {
          console.log(payload);
        });

        firebaseMessaging.getToken().then((currentToken) => {
          if (currentToken) {
            console.log(`currentToken: ${currentToken}`);
          } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
          }
        }).catch((err) => {
          console.log('An error occurred while retrieving token. ', err);
        });
      })
      .catch(function (err) {
        console.log('Unable to get permission to notify. ', err);
      });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.isLoginSuccess !== nextState.isLoginSuccess
      || true === nextState.isUpdateComponent;
  }

  handleFileChange(event) {
    const file = event.target.files[0];
    Http.uploadFile(file).then(res => {
      const fileName = res && res.images_link ? res.images_link : '';
      const { socket } = this.state;
      if (!socket) return;
      console.log('handleFileChange');
      if (this.state.userId !== '' && this.state.chanelId !== '' && (this.state.chattingUser && this.state.chattingUser.user_id !== '')) {
        const objMsg = {
          sender_id: this.state.userId,
          receiver_id: this.state.chattingUser.user_id,
          content: fileName,
          channel_id: this.state.chanelId,
          content_type: 'media'
        }
        console.log('handleFileChange _ objMsg: ' + JSON.stringify(objMsg));
        socket.emit(action.REQUEST.SEND_MESSAGE, objMsg);
      }
    });
  }

  async handleChatTo(userInfo) {
    console.log('handleChatTo ' + userInfo);
    if (!this.state.socket) return;

    const response = await Http.getChannelByToken();
    const res = response && response.channel_single ? response.channel_single : [];
    console.log('getChannelByToken');
    console.log(JSON.stringify(res));
    if (res && Array.isArray(res)) {
      let channelId = '';
      for (let i = 0; i < res.length; i++) {
        if ((res[i].member1 === this.state.userId && res[i].member2 === userInfo.user_id)
          || (res[i].member2 === this.state.userId && res[i].member1 === userInfo.user_id)) {
          channelId = res[i].channel_id;
          break;
        }
      }
      if (channelId != '') {
        this.state.socket.emit(action.REQUEST.CHAT_TO_SOMEONE, { channel_id: channelId });
        this.setState({
          chattingUser: userInfo,
          chanelId: channelId
        }, async () => {
          console.log('update ChannelId', channelId);
          const lstMessage = await Http.getListChatHistoryByChanelId(this.state.chanelId);
          this.setState({ lstMessage: lstMessage }, () => {
            console.log('handleChatTo: ' + JSON.stringify(this.state.lstMessage));
          });
        });
      } else {
        try {
          const resOrigin = await Http.createChannel(userInfo.user_id, 'single');
          console.log('createChannel');
          console.log(JSON.stringify(resOrigin));
          let res = {};
          if (resOrigin) res = JSON.parse(resOrigin);
          if (res && res.channel_id) {
            this.state.socket.emit(action.REQUEST.CHAT_TO_SOMEONE, { channel_id: res.channel_id });
            this.setState({
              chattingUser: userInfo,
              chanelId: res.channel_id
            }, async () => {
              const lstMessage = await Http.getListChatHistoryByChanelId(this.state.chanelId);
              this.setState({ lstMessage: lstMessage }, () => {
                console.log('handleChatTo: ' + JSON.stringify(this.state.lstMessage));
              });
            });
          }
        } catch (err) {
          alert('Can not create chanel');
          console.log(`handleChatTo: ${err}`);
          return;
        }
      }
    }
  }

  handleOnChange(event) {
    this.setState({ message: event.target.value });
  }

  handleSendMessage() {
    const { socket, message } = this.state;
    if (!socket) return;
    if (this.state.userId !== '' && this.state.chanelId !== '' && (this.state.chattingUser && this.state.chattingUser.user_id !== '')) {
      const objMsg = {
        sender_id: this.state.userId,
        receiver_id: this.state.chattingUser.user_id,
        content: message,
        channel_id: this.state.chanelId,
        content_type: 'text'
      }
      socket.emit(action.REQUEST.SEND_MESSAGE, objMsg);
      this.setState({ message: '' })
    } else {
      alert('Send message fail');
    }
  }

  handleLogin(response) {
    const { is_success, user_info } = response;
    if (is_success) {
      this.setState({
        isLoginSuccess: is_success,
        userId: user_info.user_id
      });
      this.state.socket.emit(action.RESPONSE.USER_INFO, {
        user_id: user_info.user_id,
        username: user_info.username
      });
      Http.getChannelByToken()
        .then(response => {
          if (response && response.channel_single) {
            const res = response.channel_single;
            console.log(`getChannelByToken: ${JSON.stringify(res)}`);
            const arr = [];
            for (let i = 0; i < res.length; i++) {
              const channelInfo = res[i];
              const userInfo = {};
              const channelId = channelInfo.channel_id;
              userInfo.user_id = channelInfo.user_id;
              userInfo.avatar = channelInfo.avatar ? userInfo.avatar : 'https://www.netclipart.com/pp/m/133-1336672_beautiful-girl-cleanser.png';
              userInfo.is_online = channelInfo.status === 'active' ? true : false;
              userInfo.username = channelInfo.fullname;
              if (Reflect.has(channelInfo, 'lastMessages')) {
                const objMsg = channelInfo.lastMessages;
                const createAt = Reflect.has(objMsg, 'create_at') ? objMsg.create_at : null;
                globalz.updateDicCreateAtOfLastSmsByChanelId(channelId, createAt);
                console.log(`globalz.updateDicCreateAtOfLastSmsByChanelId(${channelId}, ${createAt})`);
              }
              arr.push(userInfo);
            }
            this.setState({ lstUsersOnline: [...arr], isUpdateComponent: true });
          }
        })
        .catch(err => {
          alert('Can not load list users');
          console.log(err);
          return;
        });
    }
  }

  render() {
    if (!this.state.isLoginSuccess) {
      return <Login handleLogin={this.handleLogin} />
    }
    console.log('this.state.lstMessage: ' + JSON.stringify(this.state.lstMessage));
    return (
      <div className="container-fluid h-100">
        <div className="row justify-content-center h-100">
          <div className="col-md-4 col-xl-3 chat">
            <div className="card mb-sm-3 mb-md-0 contacts_card">
              <Search />
              <div className="card-body contacts_body">
                <ui className="contacts">
                  {
                    this.state.lstUsersOnline.map((e, index) => {
                      return (<User
                        key={index}
                        userId={e.user_id}
                        avatar={e.avatar}
                        username={e.username}
                        isOnline={e.is_online}
                        handleChatTo={this.handleChatTo}
                      />);
                    })
                  }
                </ui>
              </div>
              <div className="card-footer" />
            </div>
          </div>
          <div className="col-md-8 col-xl-6 chat">
            <div className="card">
              <div className="card-header msg_head">
                <div>
                  {
                    helper.isObjectEmpty(this.state.chattingUser)
                      ? <div className="img_cont"></div>
                      : (
                        <ChattingUser
                          avatar={this.state.chattingUser.avatar}
                          username={this.state.chattingUser.username}
                        />
                      )
                  }
                </div>
                <Menu />
              </div>
              <div className="card-body msg_card_body">
                {
                  helper.isObjectEmpty(this.state.chattingUser)
                    ? <div style={{ justifyContent: 'center', textAlign: 'center' }}>
                      <span>No data to display</span>
                    </div>
                    : (
                      <ChatBox
                        socket={this.state.socket}
                        senderId={this.state.userId}
                        receiverId={this.state.chattingUser.user_id}
                        chanelId={this.state.chanelId}
                        listMessage={this.state.lstMessage}
                        handleAddNewMessage={this.handleAddNewMessage}
                      />
                    )
                }
              </div>
              <div className="card-footer">
                <div className="input-group">
                  <div className="input-group-append">
                    <input id="file_input_file" type="file" value={this.state.fileInput} onChange={this.handleFileChange} />
                  </div>
                  <textarea
                    name
                    className="form-control type_msg"
                    placeholder="Type your message..."
                    defaultValue={""}
                    value={this.state.message}
                    onChange={this.handleOnChange}
                  />
                  <div className="input-group-append">
                    <span onClick={this.handleSendMessage} className="input-group-text send_btn">
                      Send
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
