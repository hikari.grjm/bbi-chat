const baseUrlAPI = 'http://chat.bbivietnam.vn/v1';

module.exports = {
    URL: {
        SOCKET: 'http://localhost:6000',

        login_bbi: `https://api.bbivietnam.vn/v3.5/guest/login/normal`,
        login: `${baseUrlAPI}/api/auth`,
        relation: `${baseUrlAPI}/api/relation`,
        refresh: `${baseUrlAPI}/api/refresh`,
        channel: `${baseUrlAPI}/api/channel`,
        messages: `${baseUrlAPI}/api/messages`,
        upload: `${baseUrlAPI}/api/upload`
    },
    IMAGE_SOURCE: 'http://chat-cdn.bbivietnam.vn/resources'
};