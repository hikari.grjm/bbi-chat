const helper = require('./helper');
const dicLastSmsByChanelId = {};


module.exports = {
    getDicCreateAtOfLastSmsByChanelId: chanelIdInput => {
        const chanelId = typeof (chanelIdInput) === 'string' ? helper.replaceKeyDic(chanelIdInput) : chanelIdInput;
        if (Reflect.has(dicLastSmsByChanelId, chanelId)) {
            console.log('getDicCreateAtOfLastSmsByChanelId' + chanelId);
            return dicLastSmsByChanelId[chanelId];
        }
        return 0;
    },
    updateDicCreateAtOfLastSmsByChanelId: (chanelIdInput, createAt) => {
        const chanelId = typeof (chanelIdInput) === 'string' ? helper.replaceKeyDic(chanelIdInput) : chanelIdInput;
        dicLastSmsByChanelId[chanelId] = createAt;
    }
}