import React from 'react';
import helper from '../common/helper';
import action from '../common/action';

import * as Http from '../utils/http';

// Child
import MessageReceiver from './Childs/MessageReceiver';
import MessageSender from './Childs/MessageSender';

export default class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lstMessage: [],
            isNewMessage: false
        }
        this.handleListMessage = this.handleListMessage.bind(this);
    }

    handleListMessage(res) {
        console.log('handleListMessage ' + JSON.stringify(res));
        if (res && Array.isArray(res)) {
            const lstData = res;
            this.setState({
                lstMessage: [],
                isNewMessage: true
            }, () => {
                const lstMsg = [];
                for (let i = 0; i < lstData.length; i++) {
                    const msg = lstData[i];
                    let objMsg = {};
                    if (Number(this.props.senderId) === Number(msg.sender_id)) {
                        objMsg = {
                            channel_id: msg.channel_id,
                            sender_id: this.props.senderId,
                            receiver_id: this.props.receiverId,
                            is_receive_msg: false,
                            avatar: 'https://www.netclipart.com/pp/m/133-1336672_beautiful-girl-cleanser.png',
                            content: msg.content,
                            content_type: msg.content_type,
                            time: helper.convertTimeDisplay(Number(msg.updated))
                        }
                    } else {
                        objMsg = {
                            channel_id: msg.channel_id,
                            sender_id: this.props.receiverId,
                            receiver_id: this.props.senderId,
                            is_receive_msg: true,
                            avatar: 'https://www.netclipart.com/pp/m/133-1336672_beautiful-girl-cleanser.png',
                            content: msg.content,
                            content_type: msg.content_type,
                            time: helper.convertTimeDisplay(Number(msg.updated))
                        }
                    }

                    if (objMsg.content_type === 'text') {

                    } else if (objMsg.content_type === 'media') {
                        const linkImage = objMsg.content;
                        objMsg.content = (<img style={{ width: 100, height: 50 }} src={linkImage} alt="image" />);
                    }

                    lstMsg.push(objMsg);
                }
                this.setState({
                    lstMessage: [...lstMsg],
                    isNewMessage: true
                })
            })
        }
    }

    async componentDidMount() {
        console.log('componentDidMount' + JSON.stringify(this.props.listMessage));
        console.log(helper.replaceKeyDic(this.props.receiverId));

        this.handleListMessage(this.props.listMessage);

        this.props.socket.on(action.RESPONSE.SEND_MESSAGE, messageInfo => {
            let newMessage = messageInfo;
            if (newMessage) {
                if (newMessage.content_type === 'text') {
                    console.log('New Message: TEXT ', messageInfo);
                    if (newMessage.sender_id === this.props.senderId) {
                        newMessage.is_receive_msg = false;
                    } else {
                        newMessage.is_receive_msg = true;
                    }
                    this.setState({
                        lstMessage: [...this.state.lstMessage, newMessage],
                        isNewMessage: true
                    });
                } else if (newMessage.content_type === 'media') {
                    console.log('New Message: MEDIA ', messageInfo);
                    if (newMessage.sender_id === this.props.senderId) {
                        newMessage.is_receive_msg = false;
                    } else {
                        newMessage.is_receive_msg = true;
                    }
                    const linkImage = newMessage.content;
                    newMessage.content = (<img style={{ width: 100, height: 50 }} src={linkImage} alt="image" />);
                    this.setState({
                        lstMessage: [...this.state.lstMessage, newMessage],
                        isNewMessage: true
                    });
                }
            }
        })
    }

    componentWillUpdate() {
        console.log('componentWillUpdate ' + JSON.stringify(this.props.listMessage));
    }

    componentDidUpdate() {
        console.log('componentDidUpdate ' + JSON.stringify(this.props.listMessage));
    }

    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps ' + JSON.stringify(nextProps.listMessage));
        this.handleListMessage(nextProps.listMessage);
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('shouldComponentUpdate ');
        console.log('nextProps.chanelId ' + JSON.stringify(nextProps.chanelId));
        console.log('this.props.chanelId ' + JSON.stringify(this.props.chanelId));
        let isUpdateComp = false;
        if (nextProps.chanelId === this.props.chanelId) {
            if (nextState.isNewMessage) {
                isUpdateComp = true;
            } else {
                isUpdateComp = false;
            }
        } else {
            isUpdateComp = true;
        }
        console.log('isUpdateComp: ' + isUpdateComp);
        return isUpdateComp;
    }

    render() {
        const { lstMessage } = this.state;
        console.log('render ' + JSON.stringify(lstMessage));
        return (
            <div>
                {
                    lstMessage.length === 0
                        ? <div>No data to display</div>
                        : lstMessage.map((e, index) => {
                            if (e) {
                                return e.is_receive_msg === true
                                    ? <MessageReceiver
                                        key={index}
                                        time={e.time}
                                        avatar={e.avatar}
                                        content={e.content}
                                    />
                                    : <MessageSender
                                        key={index}
                                        time={e.time}
                                        avatar={e.avatar}
                                        content={e.content}
                                    />;
                            }
                            return <div>No data to display</div>;
                        })
                }
            </div>
        );
    }
}