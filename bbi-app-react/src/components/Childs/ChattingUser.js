import React from 'react';

export default function ChattingUser(props) {
    return (
        <div className="d-flex bd-highlight">
            <div className="img_cont">
                <img
                    src={props.avatar}
                    className="rounded-circle user_img"
                    alt="avatar"
                />
                <span className="online_icon" />
            </div>
            <div className="user_info">
                <span>{props.username}</span>
            </div>
            <div className="video_cam">
                <span>
                    <i className="fas fa-video" />
                </span>
                <span>
                    <i className="fas fa-phone" />
                </span>
            </div>
        </div>
    );
}