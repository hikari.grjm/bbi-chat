import React from 'react';
import * as helper from '../../common/helper';

export default function ReceiverMessage(props) {
    return (
        <div className="d-flex justify-content-start mb-4">
            <div className="img_cont_msg">
                <img
                    src={props.avatar}
                    className="rounded-circle user_img_msg"
                    alt="avatar"
                />
            </div>
            <div className="msg_cotainer">
                {props.content}
                <span className="msg_time">{helper.convertTimeDisplay(props.time)}</span>
            </div>
        </div>
    );
}