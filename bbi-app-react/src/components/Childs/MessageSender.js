import React from 'react';
import helper from '../../common/helper';

export default function SenderMessage(props) {
    return (
        <div className="d-flex justify-content-end mb-4">
            <div className="msg_cotainer_send">
                {props.content}
                <span className="msg_time_send">{helper.convertTimeDisplay(props.time)}</span>
            </div>
            <div className="img_cont_msg">
                <img
                    src={props.avatar}
                    className="rounded-circle user_img_msg"
                    alt="avatar"
                />
            </div>
        </div>
    );
}