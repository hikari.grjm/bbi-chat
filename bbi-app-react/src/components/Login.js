import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import * as Http from '../utils/http';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
        this.handleOnChangeUsername = this.handleOnChangeUsername.bind(this);
        this.handleOnChangePassword = this.handleOnChangePassword.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleOnChangeUsername(event) {
        this.setState({ username: event.target.value });
    }

    handleOnChangePassword(event) {
        this.setState({ password: event.target.value });
    }

    async handleLogin() {
        const { username, password } = this.state;
        // Call /auth here
        Http.login(username, password)
            .then(userInfo => {
                if (typeof this.props.handleLogin === 'function') {
                    if (userInfo) {
                        this.props.handleLogin({
                            is_success: true,
                            user_info: {
                                user_id: userInfo.user_id,
                                username: userInfo.username
                            }
                        });
                    } else {
                        this.props.handleLogin({
                            is_success: false,
                            user_info: null
                        });
                        alert('Login fail. Please try again.');
                    }
                }
            })
            .catch(err => {
                alert('Unknow Error!');
                console.log(err);
            });
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    render() {
        return (
            <div className="Login">
                <form>
                    <Form.Group controlId="username" bsSize="large">
                        <Form.Label><strong>Username</strong></Form.Label>
                        <Form.Control
                            autoFocus
                            type="username"
                            value={this.state.username}
                            onChange={this.handleOnChangeUsername}
                        />
                    </Form.Group>
                    <Form.Group controlId="password" bsSize="large">
                        <Form.Label><strong>Password</strong></Form.Label>
                        <Form.Control
                            value={this.state.password}
                            onChange={this.handleOnChangePassword}
                            type="password"
                        />
                    </Form.Group>
                    <Button block bsSize="large" style={{ cursor: "pointer" }} disabled={!this.validateForm()} onClick={this.handleLogin}>
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}