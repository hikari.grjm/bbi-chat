import React from 'react';

export default class Menu extends React.Component {
    render() {
        return (
            <div>
                <span id="action_menu_btn">
                    <i className="fas fa-ellipsis-v" />
                    (+)
                </span>
                <div className="action_menu">
                    <ul>
                        <li>
                            <i className="fas fa-user-circle" /> View profile
                    </li>
                        <li>
                            <i className="fas fa-users" /> Add to close friends
                    </li>
                        <li>
                            <i className="fas fa-plus" /> Add to group
                    </li>
                        <li>
                            <i className="fas fa-ban" /> Block
                    </li>
                    </ul>
                </div>
            </div>
        );
    }
}