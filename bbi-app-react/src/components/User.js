import React from 'react';

export default class User extends React.Component {
    constructor(props) {
        super(props);
        this.handleChatTo = this.handleChatTo.bind(this);
    }
    handleChatTo() {
        if (typeof this.props.handleChatTo === 'function') {
            this.props.handleChatTo({
                user_id: this.props.userId,
                username: this.props.username,
                avatar: this.props.avatar
            });
        }
    }
    render() {
        const status = this.props.isOnline ? 'online' : 'offline';
        const onlineIcon = this.props.isOnline ? 'online_icon' : 'offline_icon';
        const avatar = this.props.avatar || '';
        return (
            <li onClick={this.handleChatTo} className="active" style={{ cursor: 'pointer' }}>
                <div className="d-flex bd-highlight">
                    <div className="img_cont">
                        <img alt="avatar of user"
                            src={avatar}
                            className="rounded-circle user_img"
                        />
                        <span className={onlineIcon} />
                    </div>
                    <div className="user_info">
                        <span>{this.props.username}</span>
                        <p>{status}</p>
                    </div>
                </div>
            </li>
        );
    }
}