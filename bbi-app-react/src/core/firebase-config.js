import * as firebase from "firebase";
import config from '../common/config';

export default firebase.initializeApp(config.firebase);