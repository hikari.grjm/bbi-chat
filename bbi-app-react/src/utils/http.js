import Axios from 'axios'
import { URL } from '../common/constant';
import * as globalz from '../common/global';

let RoleGroup = null
let token = null
let userInfo = null


export function uploadFile(file, onUploadProgress) {
    let formData = new FormData();
    formData.append('file', file);
    return post({
        url: URL.upload,
        data: formData,
        contentType: 'multipart/form-data',
        onUploadProgress
    }).then(res => {
        return res
    })
}

export function login(username, password) {
    return post({
        url: URL.login_bbi,
        data: {
            username: username,
            password: password
        },
        nonToken: true
    }).then(res => {
        if (!res || !res.data || !res.data.token || !res.data.token.accessToken) return null;
        return Promise.resolve(res.data.token.accessToken);
    }).then(accressToken => {
        return post({
            url: URL.login,
            token_bbi: accressToken,
            nonToken: false
        });
    }).then(res => {
        saveUserInfo(res)
        return res.userInfo;
    });
}

export function queryRoleGroup() {
    return get({
        url: URL.roleList
    }).then(res => {
        if (!res || !res.Success || !res.Data) return false
        RoleGroup = [...res.Data]
        return true
    })
}

export function getRelationship() {
    return get({
        url: URL.relation
    }).then(res => {
        if (!res) return [];
        return res;
    })
}

export function getChannelByToken() {
    return get({
        url: URL.channel
    }).then(res => {
        return res
    })
}

export function createChannel(objId, channelType) {
    return post({
        url: URL.channel,
        data: {
            object_id: objId,
            chanel_type: channelType
        }
    }).then(res => {
        return res
    })
}

export function sendMessage(content_type, senderId, channelId, body) {
    return post({
        url: `${URL.messages}?type=${content_type}&sender_id=${senderId}&channel_id=${channelId}`,
        data: {
            content: body.content,
            content_type: body.contentType,
            sender_id: body.senderId,
            channel_id: body.chanelId
        }
    }).then(res => {
        return res
    })
}

export function getListChatHistoryByChanelId(chanelId) {
    const createAt = globalz.getDicCreateAtOfLastSmsByChanelId(chanelId);
    return get({
        url: `${URL.messages}?channel_id=${chanelId}&limit=50&create_at=${createAt}`
    }).then(res => {
        if (!res) return [];
        return res;
    })
}

export function getListChatHistory() {
    return get({
        url: `${URL.messages}`
    }).then(res => {
        if (!res) return [];
        return res;
    })
}

export function getRoleGroup() {
    return [...RoleGroup]
}

export function deleteUser(username) {
    return del({
        url: URL.deleteUser + '/' + username
    }).then(res => {
        return res
    })
}

export function createUser(userInfo) {
    return post({
        url: URL.createUser,
        data: userInfo
    }).then(res => {
        return res
    })
}

export function updateUser(userInfo) {
    return put({
        url: URL.updateUser,
        data: userInfo
    }).then(res => {
        return res
    })
}

export function getCurrentUserInfo() {
    return { ...userInfo }
}

function saveUserInfo(resLogin) {
    token = resLogin.token;
    renewAccessToken(8)
}

function renewAccessToken(time) {
    setTimeout(() => {
        get({
            url: URL.refresh
        }).then(res => {
            if (!res) {
                renewAccessToken(time)
            } else {
                token = res.token;
                renewAccessToken(time)
            }
        })
    }, time * 60 * 1000 * 0.75);
}

function get({ url, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.get(
            url, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Get ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function post({ url, data, nonToken = false, contentType, onUploadProgress, token_bbi }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }
        if (contentType) {
            if (!config.headers) config.headers = {}
            config.headers['Content-Type'] = contentType
        }
        if (onUploadProgress) {
            config.onUploadProgress = onUploadProgress
        }

        Axios.post(
            url, data, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Post ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function put({ url, data, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.put(
            url, data, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Put ${url} fail: ${error}`)
            resolve(null)
        });
    })
}

function del({ url, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: "bearer " + token
                }
        }

        Axios.delete(
            url, config
        ).then((response = {}) => {
            resolve(response.data)
        }).catch((error) => {
            console.error(`Delete ${url} fail: ${error}`)
            resolve(null)
        });
    })
}