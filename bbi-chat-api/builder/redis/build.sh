#!/usr/bin/env bash
TYPE=$1
NAMESPACE=$2
#delete
kubectl delete configmap config-redis-$TYPE --namespace=$NAMESPACE
kubectl delete -f ./redis-$TYPE.yaml --namespace=$NAMESPACE
#create
kubectl create configmap config-redis-$TYPE --from-file=redis.conf --validate=false --namespace=$NAMESPACE
kubectl create -f ./redis-$TYPE.yaml --validate=false --namespace=$NAMESPACE