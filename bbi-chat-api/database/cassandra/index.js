const config = require('../../src/lib/helper').getConfig('database').local.cassandra;
const cassandra = require('cassandra-driver');
const distance = cassandra.types.distance;
const PlainTextAuthProvider = cassandra.auth.PlainTextAuthProvider;

const client = new cassandra.Client({
    contactPoints: [config.host],
    localDataCenter: config.localDataCenter,
    authProvider: new PlainTextAuthProvider(config.username, config.password),
    pooling: {
        coreConnectionsPerHost: {
            [distance.local]: 2
        },
        maxRequestsPerConnection: config.maxRequestsPerConnection
    },
    keyspace: config.keyspace
});

module.exports = () => {
    return new Promise((resolve, reject) => {
        client.connect()
            .then(() => resolve(client))
            .catch(err => reject(err));
    });
};
