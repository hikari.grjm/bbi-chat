// Sử dụng username, password _ https://hopding.com/cassandra-authentication-in-docker-container
// Mặc định thì username và password la: cassandra/cassandra. Tuy nhiên cần enable nó lên
1. docker exec -it <CONTAINTER_ID> bash
2. echo "authenticator: PasswordAuthenticator" >> /etc/cassandra/cassandra.yaml

TEST:
Dang nhap 
// Vẫn sử trong cửa sổ của docker exec ở trên

- cqlsh -u cassandra -p cassandra
- CREATE KEYSPACE IF NOT EXISTS bbi_chat WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}  AND durable_writes = true;
- USE bbi_chat;
- CREATE TABLE message (id text, content text, PRIMARY KEY (id));
