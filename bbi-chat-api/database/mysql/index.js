/* eslint-disable indent */
/* eslint-disable no-tabs */
const { getConfig } = require('../../src/lib/helper');
const { host, user, password, database } = getConfig('database').local.mysql;
const logger = require('../../src/lib/logger');
const knex = require('knex')({
	client: 'mysql',
	connection: {
		host,
		user,
		password,
		database
	},
	pool: {
		min: 0,
		max: 7
	}
});

module.exports = () => {
	return new Promise((resolve, reject) => {
		// connect to a database if needed, then pass it to `callback`:
		logger.info(`host: ${host} _ user: ${user} _ password: ${password} _ database: ${database}`);
		knex.raw('SELECT VERSION()')
			.then(() => {
				return resolve(knex);
			})
			.catch(err => {
				return reject(`Connect database MYSQL fail ${err}`);
			});
	});
};

