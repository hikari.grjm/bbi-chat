CREATE TABLE `user_info` (
    `user_id` INT(255) NOT NULL,
    `bbi_id` INT(255) NOT NULL,
    `fullname` nvarchar(50),
    `address` nvarchar(100),
    `avatar` varchar(500),
    `gender` int DEFAULT 0,
    `user_type` varchar(10),
    `status` varchar(10) DEFAULT 'active',
    PRIMARY KEY (`user_id`)
);