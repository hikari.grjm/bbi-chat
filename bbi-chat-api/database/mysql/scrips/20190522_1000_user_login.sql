CREATE TABLE `user_login` (
    `user_id` INT(255) NOT NULL,
    `username` varchar(20) NOT NULL,
    `password` varchar(200) NOT NULL,
    `created` bigint,
    PRIMARY KEY (`user_id`)
);