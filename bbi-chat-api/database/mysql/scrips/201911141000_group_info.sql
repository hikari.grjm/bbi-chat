CREATE TABLE group_info
(
    group_id INT(255) NOT NULL,
    user_create  INT(255) NOT NULL,
    group_name text NOT NULL,
    avatar varchar(500),
    updated bigint(20) DEFAULT NULL,
    PRIMARY KEY(group_id)
)
ENGINE=InnoDB;
CREATE TABLE member_mapping
(
    group_id INT(255) NOT NULL,
    member_id  INT(255) NOT NULL,
    role varchar(200) NOT NULL,
    updated bigint(20) DEFAULT NULL,
    PRIMARY KEY(group_id, member_id)
)
ENGINE=InnoDB;