CREATE TABLE channel
(
    channel_id INT(255) NOT NULL,
    member1 INT(255) NOT NULL,
    member2 INT(255) NOT NULL,
    channel_type varchar(200) NOT NULL,
    updated bigint(20) DEFAULT NULL,
    PRIMARY KEY(channel_id)
)
ENGINE=InnoDB;