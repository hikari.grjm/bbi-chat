CREATE TABLE default_messages
(
    id INT(255) NOT NULL AUTO_INCREMENT,
    bbi_id INT(255) NOT NULL,
    content nvarchar(500) NOT NULL,
    updated bigint(20) DEFAULT NULL,
    PRIMARY KEY(id)
)
ENGINE=InnoDB;