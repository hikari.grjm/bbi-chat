INSERT INTO `bbi_chat`.`user_login`(`user_id`, `username`, `password`, `created`) VALUES (7, 'guest', '$2a$10$RVtUpMA9QtTz8JeH/8dQ5eTwjp3UaBjMiw9GmqMqKIFrptiEWxW3S', 1575949657544);
INSERT INTO `bbi_chat`.`user_info`(`user_id`, `fullname`, `address`, `avatar`, `gender`, `user_type`, `status`) VALUES (7, 'guest', NULL, NULL, 0, 'guest', 'active');
INSERT INTO `bbi_chat`.`user_info`(`user_id`, `fullname`, `address`, `avatar`, `gender`, `user_type`, `status`) VALUES (8, 'admin', NULL, NULL, 0, 'normal', 'active');
INSERT INTO `bbi_chat`.`user_login`(`user_id`, `username`, `password`, `created`) VALUES (8, 'admin', '$2a$10$dYFNn0phgLruoLaLInCssO3yW/ZBCPm0Cl7jFJF8JeZg22PHOWeSG', 1575949832534);

