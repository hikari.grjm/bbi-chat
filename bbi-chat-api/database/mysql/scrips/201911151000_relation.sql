CREATE TABLE relation
(
    id INT(255) NOT NULL AUTO_INCREMENT,
    member1 INT(255) NOT NULL,
    member2 INT(255) NOT NULL,
    relation_ship varchar(200) NOT NULL,
    updated bigint(20) DEFAULT NULL,
    PRIMARY KEY(id),
    UNIQUE(user_id,object_id)
)
ENGINE=InnoDB;