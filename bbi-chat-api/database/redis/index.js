const Redis = require('ioredis');
const configRedis = require('../../src/lib/helper').getConfig('database').redis;

const redis = new Redis({
    port: configRedis.port,
    host: configRedis.host,
    family: configRedis.family,
    password: configRedis.password,
    db: configRedis.db
});

const deleteMultiKeys = (lstKeys = []) => {
    return new Promise((resolve, reject) => {
        const lstPromise = lstKeys.map(key => {
            return redis.del(key);
        });
        Promise.all(lstPromise)
            .then(() => resolve())
            .catch(err => reject(err));
    });
};

const isKeyExists = (key = '') => {
    return new Promise((resolve, reject) => {
        redis.exists(key)
            .then(res => {
                if (res && res == 1) {
                    return resolve(true);
                }
                return resolve(false);
            })
            .catch(err => reject(err));
    });
};

module.exports = () => {
    return new Promise((resolve, reject) => {
        if (redis && redis.connector && redis.connector.connecting) {
            redis.deleteMultiKeys = deleteMultiKeys;
            redis.isKeyExists = isKeyExists;
            return resolve(redis);
        }
        return reject('Connect database REDIS fail');
    });
};
