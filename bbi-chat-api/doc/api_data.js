define({ "api": [
  {
    "type": "post",
    "url": "/auth",
    "title": "Lấy token",
    "version": "0.3.0",
    "name": "GetToken",
    "group": "Auth",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Nếu người dùng có trong hệ thống sẽ trả về token. Nếu người dùng không có trong hệ thống sẽ tự động call sang hệ thống core lấy thông tin để đồng bộ. Nếu hệ thống core không có dữ liệu người dùng thì sẽ trả về mã lỗi.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Token bbi.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token của user.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Lỗi này xảy ra khi không tìm thấy user này trong hệ thống này và hệ thống core.</p>"
          }
        ]
      }
    },
    "filename": "src/api/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/refresh",
    "title": "Lấy lại token",
    "version": "0.3.0",
    "name": "RefreshToken",
    "group": "Auth",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Sau 1 khoảng thời gian nhất định(10p) token đang dùng sẽ hết hạn. Vì thế phải call lại refresh để lấy token thực thi tiếp.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>Token của user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "userInfo",
            "description": "<p>1 vài thông tin cơ bản của user.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Lỗi này xảy ra khi không tìm thấy user này trong hệ thống này và hệ thống core.</p>"
          }
        ]
      }
    },
    "filename": "src/api/refresh.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/channel",
    "title": "Tao mới 1 channel",
    "version": "0.3.0",
    "name": "CreateChannel",
    "group": "Channel",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Tạo mới channel chat</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "number",
            "optional": false,
            "field": "object_id",
            "description": "<p>Đối tượng mà user muốn tạo channel.</p>"
          },
          {
            "group": "Request body",
            "type": "string",
            "optional": false,
            "field": "token_bbi",
            "description": "<p>Token bbi.</p>"
          },
          {
            "group": "Request body",
            "type": "string",
            "optional": true,
            "field": "channel_type",
            "description": "<p>Kiểu chat(riêng hay chat nhóm nếu không truyền lên thì mặc định là chat 1 với 1).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "channel",
            "description": "<p>channel vừa tạo.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.channel_id",
            "description": "<p>Id channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.member1",
            "description": "<p>Đối tượng thứ nhất trong channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.member2",
            "description": "<p>Đối tượng thứ hai trong channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.channel_type",
            "description": "<p>Kiểu chat riêng hay chat nhóm.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.updated",
            "description": "<p>Thời gian cập nhật nhóm.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DATA_EXISTS",
            "description": "<p>2 Đối tượng đã có channel.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          }
        ]
      }
    },
    "filename": "src/api/channel.js",
    "groupTitle": "Channel"
  },
  {
    "type": "get",
    "url": "/channel",
    "title": "Lấy danh sách channel thuộc user",
    "version": "0.3.0",
    "name": "GetChannel",
    "group": "Channel",
    "permission": [
      {
        "name": "user"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/channel",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "channel",
            "description": "<p>1 mảng thông tin các channel kết quả.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.channel_id",
            "description": "<p>Id channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.member1",
            "description": "<p>Đối tượng thứ nhất trong channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "channel.member2",
            "description": "<p>Đối tượng thứ hai trong channel.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.channel_type",
            "description": "<p>Kiểu chat riêng hay chat nhóm.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.updated",
            "description": "<p>Thời gian cập nhật nhóm.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.avatar",
            "description": "<p>avatar đối tượng chat.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "channel.fullname",
            "description": "<p>đối tượng chat.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Không tìm thấy group nào.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/channel.js",
    "groupTitle": "Channel"
  },
  {
    "type": "post",
    "url": "/default-messages",
    "title": "Tạo mới tin nhắn mặc định",
    "version": "0.3.0",
    "name": "CreateMessagesDefault",
    "group": "Default_Messages",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Tạo mới Tạo mới tin nhắn mặc định</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Nội dung tin nhắn</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>tạo mới tin nhắn mặc định thành công.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          }
        ]
      }
    },
    "filename": "src/api/default_messages.js",
    "groupTitle": "Default_Messages"
  },
  {
    "type": "delete",
    "url": "/default-messages/:id",
    "title": "Xóa tin nhắn mặc định.",
    "version": "0.3.0",
    "name": "DeleteMessagesDefault",
    "group": "Default_Messages",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "contentType": "application/json",
    "parameter": {
      "fields": {
        "Request params": [
          {
            "group": "Request params",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Tin nhắn mặc định.</p>"
          }
        ]
      }
    },
    "description": "<p>Xóa tin nhắn mặc định.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Invalid Params.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Not Permission.</p>"
          }
        ]
      }
    },
    "filename": "src/api/default_messages.js",
    "groupTitle": "Default_Messages"
  },
  {
    "type": "get",
    "url": "/default-messages",
    "title": "Lấy danh sách bạn bè của 1 user",
    "version": "0.3.0",
    "name": "GetListMessagesDefault",
    "group": "Default_Messages",
    "permission": [
      {
        "name": "user"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/default_messages",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "default_messages",
            "description": "<p>1 mảng các tin nhắn mặc định được config.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "default_messages.id",
            "description": "<p>Id user.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "default_messages.bbi_id",
            "description": "<p>Id user chat bbi.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "default_messages.content",
            "description": "<p>Nội dung tin nhắn.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "default_messages.updated",
            "description": "<p>Thời gian cập nhật.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Không tìm thấy tin nhắn mặc định của user.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/default_messages.js",
    "groupTitle": "Default_Messages"
  },
  {
    "type": "put",
    "url": "/default-messages/:id",
    "title": "Cập nhật tin nhắn mặc định",
    "version": "0.3.0",
    "name": "UpdateMessagesDefault",
    "group": "Default_Messages",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "parameter": {
      "fields": {
        "Request params": [
          {
            "group": "Request params",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>của tin nhắn mặc định muốn sửa</p>"
          }
        ],
        "Request body": [
          {
            "group": "Request body",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Nội dung tin nhắn mặc định muốn sửa thành.</p>"
          }
        ]
      }
    },
    "description": "<p>Cập nhật thông tin tin nhắn mặc định.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>Cập nhật thông tin tin nhắn mặc định thành công.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DATA_NOT_CHANGED",
            "description": "<p>Không có dữ liệu thay đổi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Không có quyền chỉnh sửa thông tin tin nhắn mặc định.</p>"
          }
        ]
      }
    },
    "filename": "src/api/default_messages.js",
    "groupTitle": "Default_Messages"
  },
  {
    "type": "post",
    "url": "/messages",
    "title": "Tạo chat mới",
    "version": "0.3.0",
    "name": "CreateChat",
    "group": "Messages",
    "permission": [
      {
        "name": "user / admin"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Tạo mới chat với thành viên hoặc group(Tin nhắn media sẽ do người dùng call api, tin nhắn bình thường sẽ do socket call)</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request query": [
          {
            "group": "Request query",
            "type": "String",
            "optional": true,
            "field": "type",
            "description": "<p>Đối với tin nhắn media thì phải truyền bộ 3 query này lên. Enum(images,gift).</p>"
          },
          {
            "group": "Request query",
            "type": "number",
            "optional": true,
            "field": "sender_id",
            "description": "<p>Id người gửi tin nhắn.</p>"
          },
          {
            "group": "Request query",
            "type": "number",
            "optional": true,
            "field": "channel_id",
            "description": "<p>Id của người nhận tin nhắn.</p>"
          }
        ],
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>Nội dung tin nhắn.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "content_type",
            "description": "<p>Kiểu tin nhắn(text,audio,gift,video,icon)</p>"
          },
          {
            "group": "Request body",
            "type": "number",
            "optional": false,
            "field": "sender_id",
            "description": "<p>id người gửi</p>"
          },
          {
            "group": "Request body",
            "type": "number",
            "optional": false,
            "field": "channel_id",
            "description": "<p>id channel</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": true,
            "field": "create_at",
            "description": "<p>Thời gian gửi tin (Timestamp)</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage (nếu tin nhắn media):",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/messages?type=media&&sender_id=12121211212&&channel_id=2002",
        "type": "json"
      },
      {
        "title": "Example usage(Tin nhắn thường):",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/messages",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>tạo mới chat thành công.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Không có quyền.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi dịch vụ .</p>"
          }
        ]
      }
    },
    "filename": "src/api/messages.js",
    "groupTitle": "Messages"
  },
  {
    "type": "get",
    "url": "/messages",
    "title": "Lấy danh sách tin nhắn của user",
    "version": "0.3.0",
    "name": "GetListMessages",
    "group": "Messages",
    "permission": [
      {
        "name": "user"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/messages",
        "type": "json"
      },
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/messages?channel_id=1001&&create_at=112121212&&limit=4",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Request query": [
          {
            "group": "Request query",
            "type": "number",
            "optional": false,
            "field": "channel_id",
            "description": "<p>channel_id muốn lấy tin nhắn.</p>"
          },
          {
            "group": "Request query",
            "type": "number",
            "optional": false,
            "field": "create_at",
            "description": "<p>thời điểm muốn lấy.</p>"
          },
          {
            "group": "Request query",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>Số lượng tin nhắn muốn lấy.</p>"
          }
        ]
      }
    },
    "description": "<p>Bat buoc phai truyen tham so len.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "messases",
            "description": "<p>1 object gồm các key là đối tượng value là mảng các messages qua lại.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "messases.messages_id",
            "description": "<p>Id messages.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "messases.channel_id",
            "description": "<p>Id người nhận.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "messases.sender_id",
            "description": "<p>Id người gửi.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messases.content",
            "description": "<p>Nội dung messages.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messases.content_type",
            "description": "<p>Kiểu messages.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messases.create_at",
            "description": "<p>Thời gian gửi.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messases.status",
            "description": "<p>Trạng thái tin nhắn.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messases.updated",
            "description": "<p>Thời gian cập nhật.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Không tìm thấy mối quan hệ nào nào.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/messages.js",
    "groupTitle": "Messages"
  },
  {
    "type": "put",
    "url": "/messages/:id",
    "title": "Cập nhật trạng thái tin nhắn",
    "version": "0.3.0",
    "name": "UpdateMessage",
    "group": "Messages",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "contentType": "application/json",
    "parameter": {
      "fields": {
        "Request params": [
          {
            "group": "Request params",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>của tin nhắn.</p>"
          }
        ],
        "Request body": [
          {
            "group": "Request body",
            "type": "string",
            "optional": true,
            "field": "status",
            "description": "<p>Sửa trạng thái tin nhắn</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/messages/abcxyz",
        "type": "json"
      }
    ],
    "description": "<p>Cập nhật trạng thái tin nhắn(chỉ để admin cập nhật trạng thái(socket) - user không cần gọi).</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DATA_NOT_CHANGED",
            "description": "<p>Không có dữ liệu thay đổi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Không có quyền cập nhật.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Khi không truyền lên biến change hoặc dịch vụ bị lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/messages.js",
    "groupTitle": "Messages"
  },
  {
    "type": "post",
    "url": "/upload",
    "title": "Upload ảnh lên server",
    "version": "0.3.0",
    "name": "UploadImages",
    "group": "Upload",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Upload ảnh lên server</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage (nếu tin nhắn media):",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/upload",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>{String} [images_link] id ảnh.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Không có quyền.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi dịch vụ .</p>"
          }
        ]
      }
    },
    "filename": "src/api/upload.js",
    "groupTitle": "Upload"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Đăng kí tài khoản",
    "version": "0.3.0",
    "name": "CreateUser",
    "group": "User",
    "permission": [
      {
        "name": "user"
      }
    ],
    "contentType": "application/json",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Guest token.</p>"
          }
        ]
      }
    },
    "description": "<p>Đăng kí tài khoản</p>",
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Tên đăng nhập</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mật khẩu</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "fullname",
            "description": "<p>Tên đầy đủ</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Địa chỉ</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "avatar",
            "description": "<p>Link ảnh đại diện.</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "user_type",
            "description": "<p>Loại người dùng</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": true,
            "field": "gender",
            "description": "<p>Giới tính</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>tạo mới người dùng thành công.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          }
        ]
      }
    },
    "filename": "src/api/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user_status",
    "title": "Lấy trạng thái của user",
    "version": "0.3.0",
    "name": "GetUserStatus",
    "group": "UserStatus",
    "permission": [
      {
        "name": "user"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/user_status",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "number[]",
            "optional": false,
            "field": "list_user_id",
            "description": "<p>List user muốn lấy trạng thái.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "user_status",
            "description": "<p>1 mảng thông tin các user_status kết quả.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_status.user_id",
            "description": "<p>Id của user.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_status.status",
            "description": "<p>trạng thái.</p>"
          },
          {
            "group": "Success 200",
            "type": "number",
            "optional": false,
            "field": "user_status.updated",
            "description": "<p>Thời gian cập nhật.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_FOUND",
            "description": "<p>Không tìm thấy group nào.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/user_status.js",
    "groupTitle": "UserStatus"
  },
  {
    "type": "put",
    "url": "/user_status",
    "title": "Cập nhật trạng thái user",
    "version": "0.3.0",
    "name": "UpdateUserStatus",
    "group": "UserStatus",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "contentType": "application/json",
    "description": "<p>Tạo mới user_status chat</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Admin token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/user_status/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Request body": [
          {
            "group": "Request body",
            "type": "number",
            "optional": false,
            "field": "status",
            "description": "<p>Trạng thái muốn cập nhập.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "SUCCESS",
            "description": "<p>Cập nhật trạng thái thành công.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Lỗi này xảy ra khi dịch vụ xảy ra lỗi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          }
        ]
      }
    },
    "filename": "src/api/user_status.js",
    "groupTitle": "UserStatus"
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Cập nhật thông tin người dùng",
    "version": "0.3.0",
    "name": "UpdateUser",
    "group": "User",
    "permission": [
      {
        "name": "user / admin"
      }
    ],
    "contentType": "application/json",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Request params": [
          {
            "group": "Request params",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id người dùng.</p>"
          }
        ],
        "Request query": [
          {
            "group": "Request query",
            "type": "String",
            "optional": false,
            "field": "change",
            "description": "<p>Loại cập nhật(password or user_info).</p>"
          }
        ],
        "Request body": [
          {
            "group": "Request body",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>password mới(change = password).</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "fullname",
            "description": "<p>Tên đầy đủ (change = user_info).</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>Địa chỉ (change = user_info).</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "avatar",
            "description": "<p>Link ảnh đại diện (change = user_info).</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "user_type",
            "description": "<p>Loại người dùng (change = user_info).</p>"
          },
          {
            "group": "Request body",
            "type": "String",
            "optional": true,
            "field": "status",
            "description": "<p>trạng thái người dùng (change = user_info).</p>"
          },
          {
            "group": "Request body",
            "type": "Number",
            "optional": true,
            "field": "gender",
            "description": "<p>trạng thái người dùng (change = user_info).</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://chat.bbivietnam.vn/v1/api/user/abcxyz?change=password",
        "type": "json"
      }
    ],
    "description": "<p>Cập nhật password hoặc thông tin cá nhân người dùng.</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "DATA_NOT_CHANGED",
            "description": "<p>Không có dữ liệu thay đổi.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "INVALID_PARAMS",
            "description": "<p>Sai tham số truyền lên.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NOT_PERMISSION",
            "description": "<p>Không có quyền cập nhật.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UNKNOW_ERROR",
            "description": "<p>Khi không truyền lên biến change hoặc dịch vụ bị lỗi.</p>"
          }
        ]
      }
    },
    "filename": "src/api/users.js",
    "groupTitle": "User"
  }
] });
