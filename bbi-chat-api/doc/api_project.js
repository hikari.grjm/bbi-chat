define({
  "name": "express-es6-rest-api",
  "version": "0.3.0",
  "description": "Starter project for an ES6 RESTful Express API",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-01-17T11:13:53.713Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
