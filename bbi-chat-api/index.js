const logger = require('./src/lib/logger');
const initializeCassa = require('./database/cassandra');
const initializeMySql = require('./database/mysql');
const config = require('./src/lib/helper').getConfig('bbi-chat');

// connect to db
Promise.all([
    initializeMySql(),
    initializeCassa()
])
    .then(connection => {
        let db = null;
        let cassandra = null;
        if (connection && connection[0]) {
            db = connection[0];
            cassandra = connection[1];
        } else {
            throw new Error('initializeCassa() or MySQL: connection NULL');
        }

        const initializeServer = () => {
            const passport = require('./src/passport')(config, db, cassandra);
            const app = require('./src')(config, db, cassandra, passport);
            app.server.listen(process.env.PORT || config.port, () => {
                logger.info(`Started on port: ${app.server.address().port}`);
            });
        };
        initializeServer();
    })
    .catch(err => {
        logger.error(`CAN NOT RUN SERVER _ ERR: ${err || null}`);
    });