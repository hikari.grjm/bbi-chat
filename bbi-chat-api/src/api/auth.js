const resource = require('resource-router-middleware');
const logger = require('../lib/logger');
const helper = require('../lib/helper');
const utils = require('../lib/utils');
const lstDefaultMessages = require('../lib/helper').getConfig('default-messages');
const jwt = require('jsonwebtoken');
const modelUserStatus = require('../models/user_status');
const modelDefaultMessages = require('../models/default_messages');
const { ERROR_CODE, USER_TYPE, STATUS, TABLE } = require('../common/enum');
const httpz = require('../lib/http');

const createToken = (secretKey, userInfo) => {
    const payload = new Object();
    payload.exp = Math.floor(Date.now() / 1000) + (6000000 * 100); // 10mins
    // payload.nbf = null; // add if needed
    // payload.iat = null; // add if needed
    payload.user_id = userInfo.user_id;
    payload.bbi_id = userInfo.bbi_id;
    payload.username = userInfo.username;
    payload.user_type = userInfo.user_type;
    return jwt.sign(payload, secretKey);
};

const handleAuthByToken = (req, res, db, passport, config, tokenOutOfSystem = '', cassandra) => {
    return new Promise((resolve, reject) => {
        httpz.getCustomerProfile(tokenOutOfSystem)
            .then(data => {
                let objUserInfo = {};
                db.select([])
                    .from(TABLE.NAME.USER_INFO)
                    .where({ bbi_id: data.id })
                    .then(async rows => {
                        if (!rows.length) {
                            const maxUserId = await utils.getMaxUserId(db);
                            objUserInfo = {
                                user_id: maxUserId + 1,
                                bbi_id: data.id,
                                fullname: data.fullName,
                                address: data.address,
                                avatar: data.avatar,
                                gender: data.gender,
                                user_type: USER_TYPE.NORMAL,
                                status: STATUS.ACTIVE
                            };
                            const userStatus = {
                                user_id: data.id,
                                status: 0
                            };
                            const lstQueryExec = [
                                db(TABLE.NAME.USER_INFO).insert(objUserInfo),
                                modelUserStatus(cassandra).insert(userStatus)
                            ];
                            if (lstDefaultMessages && lstDefaultMessages.length > 0) {
                                const lstMessagesInsert = lstDefaultMessages.map(e => {
                                    const objTemp = Object.assign({}, e);
                                    objTemp.updated = new Date().getTime();
                                    objTemp.bbi_id = data.id;
                                    return objTemp;
                                });
                                lstQueryExec.push(modelDefaultMessages(db).insertMultilData(lstMessagesInsert));
                            }
                            return Promise.all(lstQueryExec);
                        }
                        const userInfo = rows[0];
                        objUserInfo = {
                            user_id: userInfo.user_id,
                            bbi_id: userInfo.bbi_id,
                            fullname: data.fullName,
                            address: data.address,
                            avatar: data.avatar,
                            gender: data.gender,
                            user_type: USER_TYPE.NORMAL,
                            status: STATUS.ACTIVE
                        };
                        return db(TABLE.NAME.USER_INFO).where({ bbi_id: userInfo.bbi_id }).update(objUserInfo, []);
                    })
                    .then(() => {
                        objUserInfo.username = data.username;
                        const attachment = helper.clone(objUserInfo);
                        const token = createToken(config.secretKey, attachment);
                        return resolve({
                            token,
                            user_info: attachment
                        });
                    })
                    .catch(error => {
                        if (error) logger.error(`PSyncUser: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            })
            .catch(err => {
                return reject(err);
            });
    });
};

const handleAuthByUsernameAndPassword = (req, res, db, passport, config) => {
    return new Promise((resolve, reject) => {
        try {
            passport.authenticate('local', { session: false }, (err, userInfo, passportMsg) => {
                if (err) {
                    if (userInfo) {
                        logger.error(`/auth _ ${userInfo.username} ERR: ${JSON.stringify(err)}`);
                    } else {
                        logger.error(`/auth _ ERR: ${JSON.stringify(err)}`);
                    }
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                }

                if (passportMsg !== ERROR_CODE.SUCCESS) {
                    return reject({ errorCode: passportMsg });
                }

                logger.info(`Login successfully with username: ${userInfo.username}`);
                const token = createToken(config.secretKey, userInfo);
                return resolve({
                    token,
                    userInfo
                });
            })(req, res);
        } catch (error) {
            logger.error(`handleAuthByUsernameAndPassword: ERR: ${error}`);
            return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
        }
    });
};

// eslint-disable-next-line no-unused-vars
module.exports = ({ config, db, passport, cassandra }) => resource({
    /** Property name to store preloaded entity on `request`. */
    id: 'auth_id',

    /**
    * @api {post} /auth Lấy token
    * @apiVersion 0.3.0
    * @apiName GetToken
    * @apiGroup Auth
    * @apiPermission user
    * @apiContentType application/json
    *
    * @apiDescription Nếu người dùng có trong hệ thống sẽ trả về token. Nếu người dùng không có trong hệ thống sẽ tự động call sang hệ thống core lấy thông tin để đồng bộ. Nếu hệ thống core không có dữ liệu người dùng thì sẽ trả về mã lỗi.
    *
    * @apiHeader {String} Authorization Token bbi.
    *
    *
    * @apiSuccess {string} token Token của user.
    *
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    * @apiError  NOT_FOUND Lỗi này xảy ra khi không tìm thấy user này trong hệ thống này và hệ thống core.
    */
    create(req, res) {
        let authorization = '';
        let token = '';
        if (Reflect.has(req, 'headers') && Reflect.has(req.headers, 'authorization') && typeof req.headers.authorization == 'string') {
            authorization = req.headers.authorization;
            token = authorization.replace(/Bearer |bearer /gi, ''); // Remove "Bearer + space"
        }

        const handleAuthentication = helper.isNullOrEmpty(authorization)
            ? handleAuthByUsernameAndPassword
            : handleAuthByToken;
        handleAuthentication(req, res, db, passport, config, token, cassandra)
            .then(objRes => res.status(200).json(objRes))
            .catch(objErr => res.status(400).json(objErr));

    }
});
