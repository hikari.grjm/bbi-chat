/* eslint-disable object-property-newline */
/* eslint-disable curly */
const resource = require('resource-router-middleware');
const model = require('../models/channel');
const { validate } = require('../lib/ajv_schema');
const createSchema = require('../schema/channel/create.json');
const { ERROR_CODE, CHANNEL_TYPE, TABLE, USER_TYPE, STATUS } = require('../common/enum');
const httpz = require('../lib/http');
const utils = require('../lib/utils');
const logger = require('../lib/logger');

module.exports = ({ db, cassandra }) => resource({
    mergeParams: true,
    id: 'channel',

    load(req, id, callback) {
        return callback(null, {});
    },

    /**
     * @api {get} /channel Lấy danh sách channel thuộc user
     * @apiVersion 0.3.0
     * @apiName GetChannel
     * @apiGroup Channel
     * @apiPermission user
     * @apiHeader {String} Authorization User token.
     *
     * @apiExample Example usage:
     * curl -i https://chat.bbivietnam.vn/v1/api/channel
     *
     * @apiSuccess {Object[]} channel 1 mảng thông tin các channel kết quả.
     * @apiSuccess {number} channel.channel_id Id channel.
     * @apiSuccess {number} channel.member1 Đối tượng thứ nhất trong channel.
     * @apiSuccess {number} channel.member2 Đối tượng thứ hai trong channel.
     * @apiSuccess {string} channel.channel_type Kiểu chat riêng hay chat nhóm.
     * @apiSuccess {string} channel.updated Thời gian cập nhật nhóm.
     * @apiSuccess {string} channel.avatar avatar đối tượng chat.
     * @apiSuccess {string} channel.fullname đối tượng chat.
     *
     * @apiError NOT_FOUND Không tìm thấy group nào.
     * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
     */

    list(req, res) {
        const user = req.user || {};
        Promise.all([
            model(db, cassandra).selectByUserId(user.bbi_id),
            model(db).selectGroupChannel(user.bbi_id)
        ])
            .then(data => {
                return res.status(200).json({ channel_single: data[0], channel_group: data[1] });
            })
            .catch(error => {
                return res.status(400).json(error);
            });
    },

    /**
    * @api {post} /channel Tao mới 1 channel
    * @apiVersion 0.3.0
    * @apiName CreateChannel
    * @apiGroup Channel
    * @apiPermission user
    * @apiContentType application/json
    *
    * @apiDescription Tạo mới channel chat
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam  (Request body) {number} object_id Đối tượng mà user muốn tạo channel.
    * @apiParam  (Request body) {string} token_bbi Token bbi.
    * @apiParam  (Request body) {string} [channel_type] Kiểu chat(riêng hay chat nhóm nếu không truyền lên thì mặc định là chat 1 với 1).
    *
    * @apiSuccess {Object[]} channel channel vừa tạo.
    * @apiSuccess {number} channel.channel_id Id channel.
    * @apiSuccess {number} channel.member1 Đối tượng thứ nhất trong channel.
    * @apiSuccess {number} channel.member2 Đối tượng thứ hai trong channel.
    * @apiSuccess {string} channel.channel_type Kiểu chat riêng hay chat nhóm.
    * @apiSuccess {string} channel.updated Thời gian cập nhật nhóm.
    *
    * @apiError  DATA_EXISTS 2 Đối tượng đã có channel.
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    */

    async create(req, res) {
        try {
            const body = req.body;
            const user = req.user;
            if (!validate(createSchema, body)) {
                return res.status(400).json({
                    errorCode: ERROR_CODE.INVALID_PARAMS
                });
            }
            if (user.bbi_id == body.object_id) {
                return res.status(400).json({
                    errorCode: ERROR_CODE.INVALID_PARAMS
                });
            }
            const rowsSelect = await db.select([]).from(TABLE.NAME.USER_INFO).where({ bbi_id: body.object_id });
            if (!rowsSelect.length) {
                const data = await httpz.getCustomerProfileById(body.token_bbi, body.object_id);
                const maxUserId = await utils.getMaxUserId(db);
                const objUserInfo = {
                    user_id: maxUserId + 1,
                    bbi_id: data.id,
                    fullname: data.fullName,
                    address: data.address,
                    avatar: data.avatar,
                    gender: data.gender,
                    user_type: USER_TYPE.NORMAL,
                    status: STATUS.ACTIVE
                };
                const wait = await db(TABLE.NAME.USER_INFO).insert(objUserInfo);
                logger.info(`Sync UserInfo ${wait}`);
            }
            const dataInsert = {
                member1: user.bbi_id,
                member2: body.object_id,
                channel_type: body.channel_type || CHANNEL_TYPE.SINGLE,
                updated: new Date().getTime()
            };
            model(db).insert(dataInsert)
                .then(data => res.status(200).json(data))
                .catch(error => res.status(400).json(error));
        } catch (error) {
            return res.status(400).json(error);
        }
    }
});

