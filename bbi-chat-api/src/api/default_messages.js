/* eslint-disable curly */
const resource = require('resource-router-middleware');
const model = require('../models/default_messages');
const { isObjectChanged, isObjectEmpty } = require('../lib/helper');
const { validate } = require('../lib/ajv_schema');
const createSchema = require('../schema/default_messages/create.json');
const updateSchema = require('../schema/default_messages/update.json');
const { ERROR_CODE } = require('../common/enum');

module.exports = ({ db }) => resource({
    mergeParams: true,
    id: 'default_messages',

    load(req, id, callback) {
        model(db).select(id)
            .then(data => {
                const info = data[0];
                if (!info) {
                    return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
                }
                return callback(null, info);
            })
            .catch(() => callback({ errorCode: ERROR_CODE.NOT_FOUND }, null));
    },

    /**
     * @api {get} /default-messages Lấy danh sách bạn bè của 1 user
     * @apiVersion 0.3.0
     * @apiName GetListMessagesDefault
     * @apiGroup Default Messages
     * @apiPermission user
     * @apiHeader {String} Authorization User token.
     *
     * @apiExample Example usage:
     * curl -i https://chat.bbivietnam.vn/v1/api/default_messages
     *
     * @apiSuccess {Object[]} default_messages 1 mảng các tin nhắn mặc định được config.
     * @apiSuccess {number} default_messages.id Id user.
     * @apiSuccess {string} default_messages.bbi_id Id user chat bbi.
     * @apiSuccess {string} default_messages.content Nội dung tin nhắn.
     * @apiSuccess {number} default_messages.updated Thời gian cập nhật.
     *
     * @apiError NOT_FOUND Không tìm thấy tin nhắn mặc định của user.
     * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
     */

    list(req, res) {
        const user = req.user || {};
        model(db).selectByUserId(user.bbi_id)
            .then(data => res.status(200).json({ default_messages: data }))
            .catch(error => res.status(400).json(error));
    },

    /**
    * @api {post} /default-messages Tạo mới tin nhắn mặc định
    * @apiVersion 0.3.0
    * @apiName CreateMessagesDefault
    * @apiGroup Default Messages
    * @apiPermission user
    * @apiContentType application/json
    *
    * @apiDescription Tạo mới Tạo mới tin nhắn mặc định
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam  (Request body) {String} content Nội dung tin nhắn
    *
    * @apiSuccess SUCCESS tạo mới tin nhắn mặc định thành công.
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    *
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    */

    create(req, res) {
        const data = req.body;
        const user = req.user;
        if (!validate(createSchema, data)) {
            return res.status(400).json({
                errorCode: ERROR_CODE.INVALID_PARAMS
            });
        }
        data.bbi_id = user.bbi_id;
        model(db).insert(data)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    },

    /**
    * @api {put} /default-messages/:id Cập nhật tin nhắn mặc định
    * @apiVersion 0.3.0
    * @apiName UpdateMessagesDefault
    * @apiGroup Default Messages
    * @apiPermission user
    * @apiContentType application/json
    *
    * @apiParam (Request params) {String} id của tin nhắn mặc định muốn sửa
    *
    * @apiDescription Cập nhật thông tin tin nhắn mặc định.
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam  (Request body) {string} content Nội dung tin nhắn mặc định muốn sửa thành.
    *
    * @apiSuccess SUCCESS Cập nhật thông tin tin nhắn mặc định thành công.
    *
    * @apiError DATA_NOT_CHANGED Không có dữ liệu thay đổi.
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    * @apiError NOT_PERMISSION Không có quyền chỉnh sửa thông tin tin nhắn mặc định.
    */
    update(req, res) {
        const objOld = req.default_messages;
        const objNew = req.body;
        const user = req.user || {};
        if (!validate(updateSchema, objNew) || isObjectEmpty(objNew)) {
            return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
        }
        if (isObjectChanged(objOld, objNew)) {
            return res.status(200).json({
                errorCode: ERROR_CODE.DATA_NOT_CHANGED
            });
        }
        if (!user || user.bbi_id != objOld.bbi_id) {
            return res.status(200).json({
                errorCode: ERROR_CODE.NOT_PERMISSION
            });
        }
        const id = objOld.id;
        model(db).update(id, [], objNew)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    },

    /**
    * @api {delete} /default-messages/:id Xóa tin nhắn mặc định.
    * @apiVersion 0.3.0
    * @apiName DeleteMessagesDefault
    * @apiGroup Default Messages
    * @apiPermission admin
    * @apiContentType application/json
    *
    * @apiParam (Request params) {String} id  Tin nhắn mặc định.
    * @apiDescription Xóa tin nhắn mặc định.
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiError INVALID_PARAMS Invalid Params.
    * @apiError NOT_PERMISSION Not Permission.
    */
    delete(req, res) {
        const objOld = req.default_messages;
        const user = req.user || {};
        if (!user || user.bbi_id != objOld.bbi_id) {
            return res.status(200).json({
                errorCode: ERROR_CODE.NOT_PERMISSION
            });
        }
        const id = objOld.id;
        model(db).delete(id)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    }
});

