/* eslint-disable object-property-newline */
/* eslint-disable no-tabs */
/* eslint-disable indent */
const { version } = require('../../package.json');
const { Router } = require('express');
const user = require('./users');
const auth = require('./auth');
const messages = require('./messages');
const channel = require('./channel');
const upload = require('./upload');
const refresh = require('./refresh');
const user_status = require('./user_status');
const default_messages = require('./default_messages');

module.exports = ({ config, db, cassandra, passport }) => {
	const api = new Router();

	// mount the facets resource
	api.use('/user', passport.authenticate('jwt', { session: false }), user({ config, db, cassandra, passport }));
	// api.use('/sync', sync({ config, db, redis, passport }));
	api.use('/auth', auth({ config, db, passport, cassandra }));
	api.use('/messages', passport.authenticate('jwt', { session: false }), messages({ config, db, cassandra, passport }));
	api.use('/channel', passport.authenticate('jwt', { session: false }), channel({ db, cassandra }));
	api.use('/upload', passport.authenticate('jwt', { session: false }), upload({ db, config }));
	api.use('/refresh', passport.authenticate('jwt', { session: false }), refresh({ config, db, passport }));
	api.use('/user_status', passport.authenticate('jwt', { session: false }), user_status({ config, db, cassandra, passport }));
	api.use('/default_messages', passport.authenticate('jwt', { session: false }), default_messages({ config, db, cassandra, passport }));

	// perhaps expose some API metadata at the root
	api.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
		res.json({ version });
	});

	return api;
};
