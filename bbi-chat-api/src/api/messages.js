/* eslint-disable curly */
const resource = require('resource-router-middleware');
const model = require('../models/messages');
const modelChannel = require('../models/channel');
const { isObjectChanged, isObjectEmpty } = require('../lib/helper');
const { upLoadFile, getMessageId } = require('../lib/utils');
const { validate } = require('../lib/ajv_schema');
const updateSchema = require('../schema/messages/update.json');
const querySchema = require('../schema/messages/query.json');
const { ERROR_CODE, message_status, CONTENT_TYPE, USER_TYPE } = require('../common/enum');

module.exports = ({ cassandra, db, config }) => resource({
    mergeParams: true,
    id: 'messages',

    load(req, id, callback) {
        model(cassandra).getById(id)
            .then(data => {
                const info = data[0];
                if (!info) {
                    return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
                }
                return callback(null, info);
            })
            .catch(() => callback({ errorCode: ERROR_CODE.NOT_FOUND }, null));
    },

    /**
     * @api {get} /messages Lấy danh sách tin nhắn của user
     * @apiVersion 0.3.0
     * @apiName GetListMessages
     * @apiGroup Messages
     * @apiPermission user
     * @apiHeader {String} Authorization User token.
     *
     * @apiExample Example usage:
     * curl -i https://chat.bbivietnam.vn/v1/api/messages
     *
     * @apiExample Example usage:
     * curl -i https://chat.bbivietnam.vn/v1/api/messages?channel_id=1001&&create_at=112121212&&limit=4
     *
     * @apiParam (Request query) {number} channel_id channel_id muốn lấy tin nhắn.
     * @apiParam (Request query) {number} create_at thời điểm muốn lấy.
     * @apiParam (Request query) {number} limit Số lượng tin nhắn muốn lấy.
     *
     * @apiDescription Bat buoc phai truyen tham so len.
     *
     * @apiSuccess {Object[]} messases 1 object gồm các key là đối tượng value là mảng các messages qua lại.
     * @apiSuccess {number} messases.messages_id Id messages.
     * @apiSuccess {number} messases.channel_id Id người nhận.
     * @apiSuccess {number} messases.sender_id Id người gửi.
     * @apiSuccess {string} messases.content Nội dung messages.
     * @apiSuccess {string} messases.content_type Kiểu messages.
     * @apiSuccess {string} messases.create_at Thời gian gửi.
     * @apiSuccess {string} messases.status Trạng thái tin nhắn.
     * @apiSuccess {string} messases.updated Thời gian cập nhật.
     *
     * @apiError NOT_FOUND Không tìm thấy mối quan hệ nào nào.
     * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
     */

    async list(req, res) {
        const query = req.query || {};
        const user = req.user || {};
        if (!query || isObjectEmpty(query)) {
            return res.status(400).json({
                errorCode: ERROR_CODE.INVALID_PARAMS
            });
        }
        // mac dinh lay theo quan he
        const lstChannel = await modelChannel(db, cassandra).selectByUserId(user.bbi_id);
        if (!lstChannel || lstChannel.length < 1) {
            return res.status(200).json([]);
        }
        if (isNaN(Number(query.create_at)) || isNaN(Number(query.limit)) || isNaN(Number(query.channel_id))) {
            return res.status(400).json({
                errorCode: ERROR_CODE.INVALID_PARAMS
            });
        }
        const create_at = Number(query.create_at);
        const limit = Number(query.limit);
        const channel_id = Number(query.channel_id);
        const fnQuery = () => Promise.resolve(model(cassandra).loading(channel_id, create_at, limit));
        fnQuery()
            .then(data => {
                for (const itemMess of data.rows) {
                    if (itemMess.content_type == CONTENT_TYPE.MEDIA) {
                        const fullPath = config.virtualPath + itemMess.content;
                        itemMess.content = fullPath;
                    }
                }
                const response =  data.rows.sort((a, b) => (a.create_at > b.create_at) ? 1 : -1);
                return res.status(200).json({ messages: response });
            })
            .catch(error => res.status(400).json(error));
    },


    read(req, res) {
        return res.status(200).json(req.messages);
    },

    /**
    * @api {post} /messages Tạo chat mới
    * @apiVersion 0.3.0
    * @apiName CreateChat
    * @apiGroup Messages
    * @apiPermission user / admin
    * @apiContentType application/json
    *
    * @apiDescription Tạo mới chat với thành viên hoặc group(Tin nhắn media sẽ do người dùng call api, tin nhắn bình thường sẽ do socket call)
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam (Request query) {String} [type] Đối với tin nhắn media thì phải truyền bộ 3 query này lên. Enum(images,gift).
    * @apiParam (Request query) {number} [sender_id] Id người gửi tin nhắn.
    * @apiParam (Request query) {number} [channel_id] Id của người nhận tin nhắn.
    *
    * @apiExample Example usage (nếu tin nhắn media):
    * curl -i https://chat.bbivietnam.vn/v1/api/messages?type=media&&sender_id=12121211212&&channel_id=2002
    * @apiExample Example usage(Tin nhắn thường):
    * curl -i https://chat.bbivietnam.vn/v1/api/messages
    *
    * @apiParam  (Request body) {String} content Nội dung tin nhắn.
    * @apiParam  (Request body) {String} content_type Kiểu tin nhắn(text,audio,gift,video,icon)
    * @apiParam  (Request body) {number} sender_id id người gửi
    * @apiParam  (Request body) {number} channel_id id channel
    * @apiParam  (Request body) {Number} [create_at] Thời gian gửi tin (Timestamp)
    *
    * @apiSuccess SUCCESS tạo mới chat thành công.
    *
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    * @apiError NOT_PERMISSION Không có quyền.
     * @apiError UNKNOW_ERROR Lỗi dịch vụ .
    */

    async create(req, res) {
        try {
            let data = {};
            const user = req.user;
            if (user.user_type != USER_TYPE.ADMIN) {
                return res.status(400).json({
                    errorCode: ERROR_CODE.NOT_PERMISSION
                });
            }
            if (req.query && !isObjectEmpty(req.query)) {
                const channel_id = Number(req.query.channel_id);
                const sender_id = Number(req.query.sender_id);
                if (isNaN(channel_id) || isNaN(sender_id)) {
                    return res.status(400).json({
                        errorCode: ERROR_CODE.INVALID_PARAMS
                    });
                }
                data.content = await upLoadFile(req);
                data.sender_id = sender_id;
                data.channel_id = channel_id;
                data.content_type = CONTENT_TYPE.MEDIA;
            } else {
                if (user.user_type != USER_TYPE.ADMIN) {
                    return res.status(400).json({
                        errorCode: ERROR_CODE.NOT_PERMISSION
                    });
                }
                data = req.body;
            }
            data.status = message_status.sent;
            data.messages_id = getMessageId(data.channel_id);
            model(cassandra).insert(data)
                .then(data => res.status(200).json(data))
                .catch(error => res.status(400).json(error));
        } catch (err) {
            return res.status(400).json(err);
        }
    },

    /**
    * @api {put} /messages/:id Cập nhật trạng thái tin nhắn
    * @apiVersion 0.3.0
    * @apiName UpdateMessage
    * @apiGroup Messages
    * @apiPermission admin
    * @apiContentType application/json
    *
    * @apiParam (Request params) {String} id của tin nhắn.
    * @apiExample Example usage:
    * curl -i https://chat.bbivietnam.vn/v1/api/messages/abcxyz
    *
    * @apiDescription Cập nhật trạng thái tin nhắn(chỉ để admin cập nhật trạng thái(socket) - user không cần gọi).
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam  (Request body) {string} [status] Sửa trạng thái tin nhắn
    *
    * @apiError DATA_NOT_CHANGED Không có dữ liệu thay đổi.
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    * @apiError NOT_PERMISSION Không có quyền cập nhật.
    * @apiError UNKNOW_ERROR Khi không truyền lên biến change hoặc dịch vụ bị lỗi.
    */
    update(req, res) {
        const objOld = req.messages;
        const objNew = req.body;
        if (!validate(updateSchema, objNew) || isObjectEmpty(objNew)) {
            return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
        }
        if (isObjectChanged(objOld, objNew)) {
            return res.status(200).json({
                errorCode: ERROR_CODE.DATA_NOT_CHANGED
            });
        }
        const messages_id = req.params.messages;
        model(cassandra).update(messages_id, objOld.channel_id.toNumber(), objNew)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    }
});

