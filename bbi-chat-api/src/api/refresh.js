const resource = require('resource-router-middleware');
const jwt = require('jsonwebtoken');
const dbUserLogin = require('../models/users');
const { getTokenInformation } = require('../lib/helper');
const { ERROR_CODE } = require('../common/enum');

// eslint-disable-next-line no-unused-vars
module.exports = ({ config, db, redis, passport }) => resource({
    mergeParams: true,
    id: 'refresh_id',

    /**
    * @api {get} /refresh Lấy lại token
    * @apiVersion 0.3.0
    * @apiName RefreshToken
    * @apiGroup Auth
    * @apiPermission user
    * @apiContentType application/json
    *
    * @apiDescription Sau 1 khoảng thời gian nhất định(10p) token đang dùng sẽ hết hạn. Vì thế phải call lại refresh để lấy token thực thi tiếp.
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiSuccess {string} token Token của user.
    * @apiSuccess {Object[]} userInfo 1 vài thông tin cơ bản của user.
    *
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    * @apiError  NOT_FOUND Lỗi này xảy ra khi không tìm thấy user này trong hệ thống này và hệ thống core.
    */
    list(req, res) {
        const { user_id } = getTokenInformation(req);
        dbUserLogin(db).selectUserByUserId(user_id)
            .then(userInfo => {
                const payload = new Object();
                payload.exp = Math.floor(Date.now() / 1000) + (60 * 10); // 10mins
                // payload.nbf = null; // add if needed
                // payload.iat = null; // add if needed
                payload.user_id = userInfo.user_id;
                payload.username = userInfo.username;
                payload.bbi_id = userInfo.bbi_id;
                payload.user_type = userInfo.user_type;
                const token = jwt.sign(payload, config.secretKey);
                return res.status(200).json({ token, userInfo });
            })
            .catch(() => {
                return res.status(400).json({ errorCode: ERROR_CODE.UNKNOW_ERROR });
            });
    }
});
