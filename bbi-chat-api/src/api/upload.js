/* eslint-disable curly */
const resource = require('resource-router-middleware');
const { uploadImages } = require('../lib/utils');

module.exports = ({ config }) => resource({
    mergeParams: true,
    id: 'messages',

    /**
    * @api {post} /upload Upload ảnh lên server
    * @apiVersion 0.3.0
    * @apiName UploadImages
    * @apiGroup Upload
    * @apiPermission admin
    * @apiContentType application/json
    *
    * @apiDescription Upload ảnh lên server
    *
    * @apiHeader {String} Authorization User token.
    *
    *
    * @apiExample Example usage (nếu tin nhắn media):
    * curl -i https://chat.bbivietnam.vn/v1/api/upload
    *
    *
    * @apiSuccess SUCCESS {String} [images_link] id ảnh.
    *
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    * @apiError NOT_PERMISSION Không có quyền.
     * @apiError UNKNOW_ERROR Lỗi dịch vụ .
    */

    create(req, res) {
        try {
            uploadImages(req)
                .then(data => {
                    const linkFull = config.virtualPath + data;
                    return res.status(200).json({ uploaded_url: linkFull })
                })
                .catch(error => res.status(400).json(error));
        } catch (error) {
            return res.status(400).json(error);
        }
    }
});

