/* eslint-disable object-property-newline */
/* eslint-disable curly */
const resource = require('resource-router-middleware');
const model = require('../models/user_status');
const { validate } = require('../lib/ajv_schema');
const querySchema = require('../schema/user_status/query.json');
const updateSchema = require('../schema/user_status/update.json');
const { ERROR_CODE } = require('../common/enum');

module.exports = ({ db, cassandra }) => resource({
    mergeParams: true,
    id: 'user_status',

    load(req, id, callback) {
        if (isNaN(Number(id))) {
            return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
        }
        model(cassandra).select(Number(id))
            .then(data => {
                const info = data[0];
                if (!info) {
                    return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
                }
                return callback(null, info);
            })
            .catch(() => callback({ errorCode: ERROR_CODE.NOT_FOUND }, null));
    },

    /**
     * @api {post} /user_status Lấy trạng thái của user
     * @apiVersion 0.3.0
     * @apiName GetUserStatus
     * @apiGroup UserStatus
     * @apiPermission user
     * @apiHeader {String} Authorization User token.
     *
     * @apiExample Example usage:
     * curl -i https://chat.bbivietnam.vn/v1/api/user_status
     *
     * @apiParam  (Request body) {number[]} list_user_id List user muốn lấy trạng thái.
     *
     * @apiSuccess {Object[]} user_status 1 mảng thông tin các user_status kết quả.
     * @apiSuccess {number} user_status.user_id Id của user.
     * @apiSuccess {number} user_status.status trạng thái.
     * @apiSuccess {number} user_status.updated Thời gian cập nhật.
     *
     * @apiError NOT_FOUND Không tìm thấy group nào.
     * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
     */

    create(req, res) {
        const data = req.body || {};
        if (!validate(querySchema, data)) {
            return res.status(400).json({
                errorCode: ERROR_CODE.INVALID_PARAMS
            });
        }
        model(cassandra).selectByUserId(data.list_user_id)
            .then(data => {
                const response = data.map(e => {
                    return {
                        user_id: Number(e.user_id),
                        status: Number(e.status),
                        updated: Number(e.updated)
                    };
                });
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(400).json(error);
            });
    },

    /**
    * @api {put} /user_status Cập nhật trạng thái user
    * @apiVersion 0.3.0
    * @apiName UpdateUserStatus
    * @apiGroup UserStatus
    * @apiPermission admin
    * @apiContentType application/json
    *
    * @apiDescription Tạo mới user_status chat
    *
    * @apiHeader {String} Authorization Admin token.
    *
    * @apiExample Example usage:
    * curl -i https://chat.bbivietnam.vn/v1/api/user_status/2
    *
    * @apiParam  (Request body) {number} status Trạng thái muốn cập nhập.
    *
    * @apiSuccess SUCCESS Cập nhật trạng thái thành công.
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    */

    update(req, res) {
        const objNew = req.body;
        if (!validate(updateSchema, objNew)) {
            return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
        }
        const user_status = req.params.user_status;
        model(cassandra).update(Number(user_status), objNew)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    }
});

