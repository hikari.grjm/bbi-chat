const resource = require('resource-router-middleware');
const modelUser = require('../models/users');
const createSchema = require('../schema/user/create.json');
const updateUserInfo = require('../schema/user/updateUserInfo.json');
const updatePassword = require('../schema/user/updatePassword.json');
const { validate } = require('../lib/ajv_schema');
const { isObjectEmpty, isNullOrEmpty } = require('../lib/helper');
const { ERROR_CODE, USER_TYPE } = require('../common/enum');

// eslint-disable-next-line no-unused-vars
module.exports = ({ config, db, cassandra }) => resource({
    mergeParams: true,
    id: 'user_id',

    load(req, id, callback) {
        if (isNaN(Number(id))) {
            return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
        }
        modelUser(db).select([], { user_id: Number(id) })
            .then(data => {
                const user = data[0];
                if (!user) {
                    return callback({ errorCode: ERROR_CODE.NOT_FOUND }, null);
                }
                return callback(null, user);
            })
            .catch(() => callback({ errorCode: ERROR_CODE.NOT_FOUND }, null));
    },
    read(req, res) {
        const user = req.user_id;
        const userResult = {
            user_id: user.user_id,
            fullname: user.fullname,
            gender: user.gender,
            avatar: user.avatar
        };
        return res.status(200).json(userResult);
    },

    /**
    * @api {post} /user Đăng kí tài khoản
    * @apiVersion 0.3.0
    * @apiName CreateUser
    * @apiGroup User
    * @apiPermission user
    * @apiContentType application/json
    * @apiHeader {String} Authorization Guest token.
    *
    * @apiDescription Đăng kí tài khoản
    *
    * @apiHeader {String} Authorization User token.
    *
    * @apiParam  (Request body) {String} username Tên đăng nhập
    * @apiParam  (Request body) {String} password Mật khẩu
    * @apiParam  (Request body) {String} [fullname] Tên đầy đủ
    * @apiParam  (Request body) {String} [address] Địa chỉ
    * @apiParam  (Request body) {String} [avatar] Link ảnh đại diện.
    * @apiParam  (Request body) {String} [user_type] Loại người dùng
    * @apiParam  (Request body) {Number} [gender] Giới tính
    *
    * @apiSuccess SUCCESS tạo mới người dùng thành công.
    *
    * @apiError  UNKNOW_ERROR Lỗi này xảy ra khi dịch vụ xảy ra lỗi.
    * @apiError INVALID_PARAMS Sai tham số truyền lên.
    */
    create(req, res) {
        const dataInsert = req.body;
        const user = req.user;
        if (user.user_type != USER_TYPE.GUEST) return res.status(400).json({ errorCode: ERROR_CODE.NOT_PERMISSION });
        if (!validate(createSchema, dataInsert)) {
            return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
        }
        dataInsert.user_type = USER_TYPE.NORMAL;
        modelUser(db, cassandra).insert(dataInsert)
            .then(data => res.status(200).json(data))
            .catch(error => res.status(400).json(error));
    },

    /**
   * @api {put} /user/:id Cập nhật thông tin người dùng
   * @apiVersion 0.3.0
   * @apiName UpdateUser
   * @apiGroup User
   * @apiPermission user / admin
   * @apiContentType application/json
   * @apiHeader {String} Authorization User token.
   *
   * @apiParam (Request params) {String} id Id người dùng.
   *
   * @apiParam (Request query) {String} change Loại cập nhật(password or user_info).
   *
   * @apiExample Example usage:
   * curl -i https://chat.bbivietnam.vn/v1/api/user/abcxyz?change=password
   *
   * @apiDescription Cập nhật password hoặc thông tin cá nhân người dùng.
   *
   * @apiHeader {String} Authorization User token.
   *
   * @apiParam  (Request body) {string} password password mới(change = password).
   * @apiParam  (Request body) {String} [fullname] Tên đầy đủ (change = user_info).
   * @apiParam  (Request body) {String} [address] Địa chỉ (change = user_info).
   * @apiParam  (Request body) {String} [avatar] Link ảnh đại diện (change = user_info).
   * @apiParam  (Request body) {String} [user_type] Loại người dùng (change = user_info).
   * @apiParam  (Request body) {String} [status] trạng thái người dùng (change = user_info).
   * @apiParam  (Request body) {Number} [gender] trạng thái người dùng (change = user_info).
   *
   * @apiError DATA_NOT_CHANGED Không có dữ liệu thay đổi.
   * @apiError INVALID_PARAMS Sai tham số truyền lên.
   * @apiError NOT_PERMISSION Không có quyền cập nhật.
   * @apiError UNKNOW_ERROR Khi không truyền lên biến change hoặc dịch vụ bị lỗi.
   */
    update(req, res) {
        const { change } = req.query;
        const user = req.user;
        if (isNaN(Number(req.params.user_id))) {
            return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
        }
        const userId = Number(req.params.user_id);
        if (!user || user.user_id != userId) {
            return res.status(400).json({ errorCode: ERROR_CODE.NOT_PERMISSION });
        }
        let fnChange = null;
        if (isNullOrEmpty(change)) {
            return res.status(400).json({ errorCode: ERROR_CODE.UNKNOW_ERROR });
        }
        if (change.toUpperCase() == 'PASSWORD') {
            if (!validate(updatePassword, req.body) || isObjectEmpty(req.body)) {
                return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
            }
            fnChange = modelUser(db).updatePassword;
        } else if (change.toUpperCase() == 'USER_INFO') {
            if (!validate(updateUserInfo, req.body) || isObjectEmpty(req.body)) {
                return res.status(400).json({ errorCode: ERROR_CODE.INVALID_PARAMS });
            }
            fnChange = modelUser(db).updateUserInfo;
        } else {
            return res.status(400).json({ errorCode: ERROR_CODE.UNKNOW_ERROR });
        }

        fnChange(userId, [], req.body)
            .then(data => {
                if (data) {
                    return res.status(200).json(data);
                }
                return res.status(200).json();
            })
            .catch(error => res.status(400).json(error));
    }
});
