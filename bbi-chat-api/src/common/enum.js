module.exports.USER_TYPE = {
    NORMAL: 'normal',
    ADMIN: 'admin',
    GUEST: 'guest'
};

module.exports.MOMENT = {
    MONTHS: 'months',
    DAYS: 'days'
};
module.exports.Node = {
    node1: 'node1',
    node2: 'node2'
};

module.exports.TWENTY_DAYS = 1000 * 60 * 60 * 24 * 20;

module.exports.ERROR_CODE = {
    SUCCESS: 'SUCCESS',
    BODY_IS_NULL: 'BODY_IS_NULL',
    PHONE_NUMBER_IS_REQUIRE: 'PHONE_NUMBER_IS_REQUIRE',
    CONTENT_IS_REQUIRE: 'CONTENT_IS_REQUIRE',
    CONTENT_IS_OUT_OF_RANGE: 'CONTENT_IS_OUT_OF_RANGE',
    PACKAGE_ID_IS_REQUIRE: 'PACKAGE_ID_IS_REQUIRE',
    PARAMS_IS_REQUIRE: 'PARAMS_IS_REQUIRE',
    PAGE_IS_REQUIRE: 'PAGE_IS_REQUIRE',
    CONTENT_NOT_CONTAIN_SALE_ORDER_ID: 'CONTENT_NOT_CONTAIN_SALE_ORDER_ID',
    SALE_ORDER_ID_IS_REQUIRE: 'SALE_ORDER_ID_IS_REQUIRE',
    INVALID_PARAMS: 'INVALID_PARAMS',
    DATA_EXISTS: 'DATA_EXISTS',
    NOT_FOUND: 'NOT_FOUND',
    NOT_PERMISSION: 'NOT_PERMISSION',
    DATA_NOT_CHANGED: 'DATA_NOT_CHANGED',
    OVER_SMS_ALLOWED_ON_PACKAGE: 'OVER_SMS_ALLOWED_ON_PACKAGE',
    PARAM_IS_OUT_OF_RANGE: 'PARAM_IS_OUT_OF_RANGE',
    UNKNOW_ERROR: 'UNKNOW_ERROR',
    DATABASE_ERROR: 'DATABASE_ERROR',
    GREATER_THAN_CURRENT_DAY: 'GREATER_THAN_CURRENT_DAY',
    FILE_IS_NOT_IMAGES: 'FILE_IS_NOT_IMAGES',
    FILE_ERROR: 'FILE_ERROR'
};

module.exports.ROLE = {
    ADMIN: 'ADMIN',
    USER: 'USER'
};

module.exports.OPERATOR = {
    IsEqualTo: '=',
    IsNotEqualTo: '!=',
    IsIn: 'IN',
    IsLessThan: '<',
    IsMoreThan: '>',
    IsLessThanOrIsEqual: '<=',
    IsMoreThanOrIsEqual: '>=',
    LIKE: 'LIKE'
};

module.exports.PARAMS_URL = {
    CLIENT: 'client',
    PARTNER: 'partner',
    DETAIL: 'detail',
    FULL: 'full'
};

module.exports.STATUS = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
};

module.exports.message_status = {
    sent: 'sent',
    seen: 'seen'
};
module.exports.CONTENT_TYPE = {
    TEXT: 'text',
    MEDIA: 'media'
};
module.exports.RELATION = {
    FRIEND: 'friend'
};
module.exports.CHANNEL_TYPE = {
    SINGLE: 'single',
    GROUP: 'group'
};

module.exports.PACKAGE_ON_SYSTEM = {
    PACKAGE_1: 'package_1',
    PACKAGE_2: 'package_2',
    PACKAGE_3: 'package_3'
};

module.exports.PACKAGE_ON_PARTNER = {
    PACKAGE_1: 1,
    PACKAGE_2: 2,
    PACKAGE_3: 3
};
module.exports.RESPONSE_STATUS = {
    SUCCESS: 0,
    ERROR: 1
};

module.exports.NETWORK_OPERATOR = {
    VIETTEL: 'VIETTEL',
    VINAPHONE: 'VINAPHONE',
    MOBIPHONE: 'MOBIPHONE',
    VIETNAMOBILE: 'VIETNAMOBILE',
    UNKNOW: 'UNKNOW'
};
module.exports.PARAM_URL_LENGTH = 250;

module.exports.TABLE = {
    NAME: {
        USER_LOGIN: 'user_login',
        USER_INFO: 'user_info',
        MESSAGES: 'messages',
        GROUP_INFO: 'group_info',
        MEMBER_MAPPING: 'member_mapping',
        RELATION: 'relation',
        CHANNEL: 'channel',
        DEFAULT_MESSAGES: 'default_messages',
        USER_STATUS: 'user_status'
    },
    FIELDS: {
        USER_LOGIN: {
            USER_ID: 'user_login.user_id',
            USERNAME: 'username',
            PASSWORD: 'password',
            CREATED: 'created'
        },
        USER_INFO: {
            USER_ID: 'user_info.user_id',
            BBI_ID: 'bbi_id',
            FULLNAME: 'fullname',
            ADDRESS: 'address',
            EMAIL: 'email',
            PHONE: 'phone',
            USER_TYPE: 'user_type',
            STATUS: 'status'
        },
        GROUP_INFO: {
            GROUP_ID: 'group_id'
        },
        MEMBER_MAPPING: {
            GROUP_ID: 'group_id',
            MEMBER_ID: 'member_id'
        },
        MESSAGES: {
            ID: 'messages_id',
            SENDER_ID: 'sender_id',
            CHANNEL_ID: 'channel_id'
        },
        RELATION: {
            OBJECT_ID: 'object_id'
        },
        CHANNEL: {
            CHANNEL_ID: 'channel_id',
            MEMBER1: 'member1',
            MEMBER2: 'member2'
        },
        NODE: 'node',
        BBI_ID: 'bbi_id'
    }
};
