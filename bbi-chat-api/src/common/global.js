/**
 *
 * LIST GLOBAL DICTIONARIES
 *
 * DicUserMappingAccount[user_id] = account_id;
 * DicAccountMappingUser[account_id] = user_id;
 *
*/

const helper = require('../lib/helper');

module.exports = {
    updateGlobalDictionary: (dictionary, keyInp = '', valueInp) => {
        const key = helper.replaceKeyDic(keyInp);
        if (!Reflect.has(global, dictionary)) {
            global[dictionary] = {};
        }
        global[dictionary][key] = valueInp;
    },
    getGlobalDictionary: (dictionary, keyInp = '') => {
        const key = helper.replaceKeyDic(keyInp);
        if (!key) {
            return global[dictionary];
        }
        return global[dictionary] && global[dictionary][key] ? global[dictionary][key] : null;
    },
    clearGlobalDictionary: (dictionary, keyInp = '') => {
        const key = helper.replaceKeyDic(keyInp);
        if (key) {
            global[dictionary][key] = {};
        }
    },
    isKeyExistOnDictionary: (dictionary, keyInp = '') => {
        const key = helper.replaceKeyDic(keyInp);
        if (Reflect.has(global[dictionary], key)) {
            return true;
        }
        return false;
    }
};