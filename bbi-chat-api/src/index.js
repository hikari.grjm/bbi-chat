/* eslint-disable object-property-newline */
/* eslint-disable no-tabs */
/* eslint-disable indent */
const http = require('http');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const middleware = require('./middleware');
const api = require('./api');
app.server = http.createServer(app);

module.exports = (config, db, cassandra, passport) => {
	// logger
	app.use(morgan('dev'));
	// 3rd party middleware
	app.use(cors({ exposedHeaders: config.corsHeaders }));
	app.use(bodyParser.json({ limit: config.bodyLimit }));
	// internal middleware
	app.use(middleware({ config, db }));
	// api router
	app.use('/api', api({ config, db, cassandra, passport }));
	return app;
};
