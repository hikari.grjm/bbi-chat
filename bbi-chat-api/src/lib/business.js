const { TWENTY_DAYS } = require('../common/enum');
const logger = require('./logger');
const { getConfig, getMonthOnly } = require('./helper');
const utils = require('./utils');
const smsPackageInfo = getConfig('sms-package');
const smsSingleInfo = getConfig('sms-single');


module.exports.isOver20days = (startedPackageTime = null, createdSmsTime = null) => {
    const endedPackageTime = startedPackageTime + TWENTY_DAYS;
    if (createdSmsTime <= endedPackageTime) {
        return false;
    }
    return true;
};

module.exports.isInMonth = (firstDateInp = null, secondDateInp = null) => {
    const monthOfFirstDate = getMonthOnly(firstDateInp);
    const monthOfSecondDate = getMonthOnly(secondDateInp);
    if (monthOfFirstDate == monthOfSecondDate) {
        return true;
    }
    return false;
};

module.exports.getPackageInfo = packageId => {
    let packageInfo = {};
    if (Reflect.has(smsPackageInfo, `package_${packageId}`)) {
        packageInfo = smsPackageInfo[`package_${packageId}`];
    } else {
        packageInfo = {
            limit: 0,
            price: 0,
            origin_price: 0
        };
        logger.info(`Can not get PackageInfo of package_id = ${packageId}`);
    }
    return packageInfo;
};

module.exports.getSingleInfo = (smsInfo = {}) => {
    const networkOperator = utils.getNetworkOperator(smsInfo.phone_number).toLowerCase();
    let singleInfo = 0;
    if (Reflect.has(smsSingleInfo, networkOperator)) {
        singleInfo = smsSingleInfo[networkOperator];
    } else {
        singleInfo = 0;
        logger.info(`Can not get SMS Single Info of number = ${smsInfo.phone_number}`);
    }
    return singleInfo;
};