/* eslint-disable prefer-template */
module.exports.QueryRender = {
    SelectIn: (table = '', priKey = '', arrQuery = []) => {
        let value = '';
        for (const item of arrQuery) {
            if (isNumber(item)) {
                value += `${item},`;
            } else {
                value += `'${item}',`;
            }
        }
        value = value.substring(0, value.length - 1);
        const queryResult = `SELECT * FROM ${table} WHERE ${priKey} IN (${value})`;
        return queryResult;
    },
    Insert: (table = '', objectInsert = {}) => {
        let field = '';
        let value = '';
        for (const property in objectInsert) {
            field += `${property},`;
            if (isNumber(objectInsert[property])) {
                value += `${objectInsert[property]},`;
            } else {
                value += `'${objectInsert[property]}',`;
            }
        }
        field = field.substring(0, field.length - 1);
        value = value.substring(0, value.length - 1);
        const queryResult = `INSERT INTO ${table} (${field}) VALUES (${value})`;
        return queryResult;
    },
    Update: (table = '', objectKey = {}, objectInsert = {}) => {
        let UpdateField = '';
        let key = '';
        let AND = '';
        for (const property in objectInsert) {
            if (isNumber(objectInsert[property])) {
                UpdateField += `${property}=${objectInsert[property]},`;
            } else {
                UpdateField += `${property}='${objectInsert[property]}',`;
            }
        }
        for (const property in objectKey) {
            if (isNumber(objectKey[property])) {
                key += `${AND} ${property}=${objectKey[property]}`;
            } else {
                key += `${AND} ${property}='${objectKey[property]}'`;
            }
            AND = 'AND';
        }
        UpdateField = UpdateField.substring(0, UpdateField.length - 1);
        const queryResult = `UPDATE ${table} set ${UpdateField} WHERE ${key}`;
        return queryResult;
    }
};
function isNumber(n) {
    return typeof n == 'number' && !isNaN(n) && isFinite(n);
}