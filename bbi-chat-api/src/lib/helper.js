/* eslint-disable object-property-newline */
/* eslint-disable handle-callback-err */
// const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const path = require('path');
const logger = require('./logger');
const moment = require('moment');

const parseConfig = (urlCloud, urlDebug) => {
    try {
        const exist = fs.existsSync(urlCloud);
        if (exist) {
            const data = fs.readFileSync(urlCloud, 'utf8');
            if (data) {
                return JSON.parse(data);
            }
        } else {
            const existDebug = fs.existsSync(path.join(urlDebug));
            if (existDebug) {
                const data = fs.readFileSync(path.join(urlDebug), 'utf8');
                if (data) {
                    return JSON.parse(data);
                }
            }
        }
    } catch (error) {
        logger.error(`invalid config: ${error}`);
    }
    return null;
};

module.exports = {
    bcryptHash: plainText => {
        const salt = bcrypt.genSaltSync(10);
        const hashText = bcrypt.hashSync(plainText, salt);
        return hashText;
    },
    bcryptCompare: (plainText, hashText) => {
        return bcrypt.compareSync(plainText, hashText);
    },
    isNullOrEmpty: data => {
        if (!data) return true;
        let output = data;

        if (typeof output !== 'string') {
            output = output.toString();
        }
        output = output.trim();

        return output.length <= 0;
    },
    removeNewLine: strInput => {
        const strReturn = strInput.replace(/\r|\n|\t/g, ' ');
        return strReturn;
    },
    replaceString: valueInput => {
        return valueInput.replace(/\\/g, '\\\\')
            .replace(/\$/g, '\\$')
            .replace(/'/g, "\\'")
            .replace(/"/g, '\\"');
    },
    replaceKeyDic: valueInput => {
        return valueInput.replace(/-/g, '');
    },
    getNullable: data => {
        if (data === '' || data === false || data === 0) return data;
        if (!data) return null;
        return data;
    },
    getBoolean: data => {
        if (data === '' || data === false || data === 0 || data == '0') return false;
        if (!data) return false;
        return true;
    },
    clone: data => {
        return JSON.parse(JSON.stringify(data));
    },
    isObjectChanged: (oldObj = {}, newObj = {}) => {
        let isSame = true;
        for (const key in newObj) {
            if (!Reflect.has(oldObj, key)) continue;
            if (oldObj[key] !== newObj[key]) {
                isSame = false;
            }
        }
        return isSame;
    },
    isObjectEmpty: obj => {
        for (const key in obj) {
            if (Reflect.has(obj, key)) {
                return false;
            }
        }
        return true;
    },
    getConfig: name => {
        try {
            const pathCloudConfig = `/etc/${name}/config-${name}.json`;
            const pathDebugConfig = `${process.cwd()}/config/config-${name}.json`;
            const content = parseConfig(pathCloudConfig, pathDebugConfig);
            return content;
        } catch (error) {
            logger.error(`getConfig _ ERR: ${error}`);
        }
        return null;
    },
    getTokenInformation: request => {
        if (request.user) {
            return request.user;
        }
        return {};
    },
    getStartOfDate: dateTimeInp => {
        return moment(dateTimeInp).utc().startOf('day').valueOf();
    },
    getEndOfDate: dateTimeInp => {
        return moment(dateTimeInp).utc().endOf('day').valueOf();
    },
    getPrevDate: () => {
        return moment().utc().endOf('day').valueOf() - 86400000;
    },
    getCurrentDate: (time = null) => {
        if (time != null) {
            return moment(time).utc().startOf('day').valueOf();
        }
        return moment().utc().startOf('day').valueOf();
    },
    getCurrentTime: () => {
        return moment().utc().valueOf();
    },
    getDateOnly: dateTimeInp => {
        return moment(dateTimeInp).utc().startOf('day').valueOf();
    },
    getMonthOnly: dateTimeInp => {
        return moment(dateTimeInp).utc().month() + 1;
    },
    getStartOfMonth: (timeInput, isGetDateOnly = false) => {
        const timeOutput = moment(timeInput).utc().startOf('month').valueOf();
        if (isGetDateOnly) {
            return moment(timeOutput).utc().startOf('day').valueOf();
        }
        return timeOutput;
    },
    getEndOfMonth: (timeInput, isGetDateOnly = false) => {
        const timeOutput = moment(timeInput).utc().endOf('month').valueOf();
        if (isGetDateOnly) {
            return moment(timeOutput).utc().startOf('day').valueOf();
        }
        return timeOutput;
    },
    getNearestTime: (time, type, quantity) => {
        // VD: time = 2019/09/10 | type = months | quantity = 1
        // => Return: 2019/08/10
        return moment(time).utc().subtract(quantity, type).valueOf();
    },
    isGreaterCurrentDate: time => {
        const currentDate = moment().utc().startOf('day').valueOf();
        if (time > currentDate) {
            return true;
        }
        return false;
    },
    isInDay: time => {
        const currentTime = this.getCurrentTime();
        const startOfDay = this.getStartOfDate(currentTime);
        const endOfDay = this.getEndOfDate(currentTime);
        if (startOfDay <= time && time <= endOfDay) {
            return true;
        }
        return false;
    }
};
