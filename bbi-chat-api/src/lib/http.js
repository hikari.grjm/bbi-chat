const axios = require('axios');
const { getConfig } = require('./helper');
const configBBIChat = getConfig('bbi-chat');
const { ERROR_CODE } = require('../common/enum');
const logger = require('./logger');

module.exports.getCustomerProfile = token => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: configBBIChat.urlCustomerProfile,
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(res => {
                if (res && res.data && res.data.data) {
                    const data = res.data.data;
                    if (!data.id || !data.username) {
                        logger.error(`getCustomerProfile: ERROR user_id || username NULL `);
                        return reject({ errorCode: ERROR_CODE.NOT_FOUND });
                    }
                    return resolve(data);
                }
                return reject({ errorCode: ERROR_CODE.NOT_FOUND });
            })
            .catch(err => {
                logger.error(`getCustomerProfile: ERROR ` + err);
                return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
            });
    });
};
module.exports.getCustomerProfileById = (token, id) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'get',
            url: `${configBBIChat.urlGetCustomerById}?id=${id}`,
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(res => {
                if (res && res.data && res.data.data) {
                    const data = res.data.data;
                    if (!data.id) {
                        logger.error(`getCustomerProfileById: ERROR user_id || username NULL `);
                        return reject({ errorCode: ERROR_CODE.NOT_FOUND });
                    }
                    return resolve(data);
                }
                return reject({ errorCode: ERROR_CODE.NOT_FOUND });
            })
            .catch(err => {
                logger.error(`getCustomerProfile: ERROR ` + err);
                return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
            });
    });
};
