/* eslint-disable no-console */
const request = require('request');
const { getConfig } = require('./helper');
const configTelegram = getConfig('logger').telegram;
const chatId = configTelegram.chat_id;
const serverUrl = configTelegram.uri;

const sendToTelegram = msg => {
    if (!serverUrl || !chatId || !configTelegram.enable) {
        return;
    }
    const options = {
        uri: serverUrl,
        method: 'POST',
        json: {
            chat_id: chatId,
            text: msg
        }
    };
    request(options);
    return;
};

module.exports = {
    toString: msg => {
        if (typeof msg != 'string') {
            return JSON.stringify(msg);
        }
        return msg;
    },
    info: msg => {
        sendToTelegram(`API INFO: ${msg}`);
        return console.log(msg);
    },
    error: msg => {
        sendToTelegram(`API ERROR: ${msg}`);
        return console.error(msg);
    }
};