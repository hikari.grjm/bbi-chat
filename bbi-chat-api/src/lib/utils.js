/* eslint-disable no-tabs */
/* eslint-disable indent */

const uuid = require('uuid');
const { getConfig } = require('./helper');
const config = getConfig('bbi-chat');
const helper = require('./helper');
const globalz = require('../common/global');
const { RESPONSE_STATUS, ERROR_CODE } = require('../common/enum');
const axios = require('axios');
const formidable = require('formidable');
const fs = require('fs');
const { CONTENT_TYPE, NETWORK_OPERATOR, RANGE_OF_CONTENT, DICTIONARY, TABLE } = require('../common/enum');

// eslint-disable-next-line valid-jsdoc
/**	Creates a callback that proxies node callback style arguments to an Express Response object.
 *	@param {express.Response} res	Express HTTP Response
 *	@param {number} [status=200]	Status code to send on success
 *
 *	@example
 *		list(req, res) {
 *			collection.find({}, toRes(res));
 *		}
 */
module.exports.toRes = (res, status = 200) => {
	return (err, thing) => {
		if (err) return res.status(500).send(err);

		if (thing && typeof thing.toObject === 'function') {
			// eslint-disable-next-line no-param-reassign
			thing = thing.toObject();
		}
		res.status(status).json(thing);
	};
};
module.exports.checkVietnamese = dataContent => {
	// return 0;
	const contain = /[àáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]/i.test(dataContent);
	return contain ? CONTENT_TYPE.VIETNAMESE : CONTENT_TYPE.NORMAL;
};
module.exports.checkRangeOfContent = (content, content_type) => {
	if ((content_type == CONTENT_TYPE.NORMAL && content.length > RANGE_OF_CONTENT.NORMAL) ||
		(content_type == CONTENT_TYPE.VIETNAMESE && content.length > RANGE_OF_CONTENT.VIETNAMESE)
	) {
		return false;
	}
	return true;
};
module.exports.getNetworkOperator = (phoneNumber = '') => {
	const dicNetworkOperator = config.network_operator;
	const headNumber = phoneNumber.slice(0, 3);
	let networkOperator = '';
	if (dicNetworkOperator.viettel.includes(headNumber)) {
		networkOperator = NETWORK_OPERATOR.VIETTEL;
	} else if (dicNetworkOperator.vinaphone.includes(headNumber)) {
		networkOperator = NETWORK_OPERATOR.VINAPHONE;
	} else if (dicNetworkOperator.mobiphone.includes(headNumber)) {
		networkOperator = NETWORK_OPERATOR.MOBIPHONE;
	} else if (dicNetworkOperator.vietnamobile.includes(headNumber)) {
		networkOperator = NETWORK_OPERATOR.VIETNAMOBILE;
	} else {
		networkOperator = NETWORK_OPERATOR.UNKNOW;
	}
	return networkOperator;
};

module.exports.genUUID = () => {
	return `${uuid()}${new Date().getTime()}`;
};

module.exports.generateRandomInteger = (min, max) => {
	return Math.floor(min + Math.random() * (max + 1 - min));
};
module.exports.getTimestamp = () => {
	return new Date().getTime() - new Date('2019-01-01').getTime();
};
module.exports.getMaxUserId = db => {
	return new Promise((resolve, reject) => {
		db(TABLE.NAME.USER_INFO).max(TABLE.FIELDS.USER_INFO.USER_ID)
			.then(max => {
				const keys = Object.keys(max[0])[0];
				return resolve(max[0][keys]);
			})
			.catch(err => {
				return reject(err);
			});
	});
};
module.exports.getMaxChannelId = db => {
	return new Promise((resolve, reject) => {
		db(TABLE.NAME.CHANNEL).max(TABLE.FIELDS.CHANNEL.CHANNEL_ID)
			.then(max => {
				const keys = Object.keys(max[0])[0];
				return resolve(max[0][keys]);
			})
			.catch(err => {
				return reject(err);
			});
	});
};
module.exports.getMessageId = channel_id => {
	const idString = `${channel_id}${this.getTimestamp()}`;
	return Number(idString);
};

module.exports.upLoadFile = req => {
	return new Promise((resolve, reject) => {
		try {
			const form = new formidable.IncomingForm();
			form.uploadDir = config.mediaFolder;
			form.parse(req, (err, fields, files) => {
				if (err) return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
				const fileUpload = files.file;
				if (!fileUpload) return reject({ errorCode: ERROR_CODE.FILE_ERROR });
				// Lấy ra đường dẫn tạm của tệp tin trên server
				const tmpPath = fileUpload.path;
				const typeFile = fileUpload.name.slice(fileUpload.name.indexOf('.'), fileUpload.name.length);
				const fileName = this.genUUID();
				// Khởi tạo đường dẫn mới, mục đích để lưu file vào thư mục uploads của chúng ta
				const pathResult = fileName + typeFile;
				const newPath = `${config.mediaFolder}/${fileName}${typeFile}`;
				// Đổi tên của file tạm thành tên mới và lưu lại
				fs.rename(tmpPath, newPath, err => {
					if (err) throw err;
					return resolve(pathResult);
				});
			});
		} catch (err) {
			console.log(`err uploadFile${JSON.stringify(err)}`);
			return reject(err);
		}
	});
};
module.exports.uploadImages = req => {
	return new Promise((resolve, reject) => {
		try {
			const form = new formidable.IncomingForm();
			form.uploadDir = config.mediaFolder;
			form.parse(req, (err, fields, files) => {
				if (err) return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
				const fileUpload = files.file;
				const fileType = files.type;
				if (!fileUpload) return reject({ errorCode: ERROR_CODE.FILE_ERROR });
				// Lấy ra đường dẫn tạm của tệp tin trên server
				const tmpPath = fileUpload.path;
				const typeFile = fileUpload.name.slice(fileUpload.name.indexOf('.'), fileUpload.name.length);
				const fileName = this.genUUID();
				// Khởi tạo đường dẫn mới, mục đích để lưu file vào thư mục uploads của chúng ta
				const pathResult = fileName + typeFile;
				const newPath = `${config.mediaFolder}/${fileName}${typeFile}`;
				// Đổi tên của file tạm thành tên mới và lưu lại
				fs.rename(tmpPath, newPath, err => {
					if (err) throw err;
					return resolve(pathResult);
				});
			});
		} catch (err) {
			console.log(`err uploadFile${JSON.stringify(err)}`);
			return reject(err);
		}
	});
};
module.exports.getInformationUser = (username, password) => {
	return new Promise((resolve, reject) => {
		try {
			axios({
				method: 'post',
				url: config.addressGetInfoUser,
				data: {
					username,
					password
				}
			})
				.then(res => {
					if (res.data != null && res.data.status == RESPONSE_STATUS.SUCCESS) {
						return resolve(res.data);
					}
					return reject();
				});
		} catch (err) {
			return reject(err);
		}
	});
};
