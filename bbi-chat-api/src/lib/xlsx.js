const fs = require('fs');
const Excel = require('exceljs');

// const arrDataTest = [
//     {
//         fullname: 'Khổng Thế Anh',
//         package_id: 1,
//         number_of_sms: 3,
//         fees: 12000,
//         total_number_of_sms: 9,
//         total_fees: 5000
//     }
// ];

module.exports.createFeesXlsx = (fromDate, toDate, arrData, email, isParner) => {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('Report', {
        pageSetup: {
            paperSize: 9,
            orientation: 'landscape'
        }
    });

    /* TITLE*/
    // worksheet.mergeCells('C1', 'J2');
    // worksheet.getCell('C1').value = 'SMS ORDERS REPORT';
    worksheet.getCell('A1').value = `From Date: ${fromDate}`;
    worksheet.getCell('A1').font = {
        size: 11.5,
        bold: true
    };
    worksheet.getCell('A2').value = `To Date: ${toDate}`;
    worksheet.getCell('A2').font = {
        size: 11.5,
        bold: true
    };

    /* Column headers*/
    worksheet.getRow(4).values = ['Fullname', 'Package ID', 'Number of SMS', 'Fees'];
    worksheet.getRow(4).font = {
        size: 11.5,
        bold: true
    };

    worksheet.columns = [
        {
            key: 'fullname',
            width: 35
        },
        {
            key: 'package_id',
            width: 13
        },
        {
            key: 'number_of_sms',
            width: 17
        },
        {
            key: 'fees',
            width: 15
        }
    ];

    let prevFullname = '';
    let curFullname = '';
    const endData = arrData.length - 1;
    arrData.forEach((item, index) => {
        prevFullname = index > 0 ? arrData[index - 1].fullname : arrData[index].fullname;
        curFullname = index > 0 ? arrData[index].fullname : arrData[index].fullname;
        const row = {
            fullname: item.fullname,
            package_id: item.package_id,
            number_of_sms: item.number_of_sms,
            fees: item.fees
        };
        const rowTotal = {
            fullname: 'TOTAL',
            package_id: '',
            number_of_sms: item.total_number_of_sms,
            fees: item.total_fees
        };
        if (prevFullname != curFullname) worksheet.addRow(rowTotal);
        worksheet.addRow(row);
        if (index == endData) worksheet.addRow(rowTotal);
    });

    const borderStyles = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
    };

    let startFlag = 0;
    worksheet.eachRow({ includeEmpty: true }, (rowz, rowNumber) => {
        const row = rowz;
        if (row.values.includes('Fullname')) {
            startFlag = rowNumber + 1;
        }
        row.alignment = { vertical: 'middle' };
        // eslint-disable-next-line no-unused-vars
        row.eachCell({ includeEmpty: true }, (cellz, colNumber) => {
            const cell = cellz;
            if (cell.address != 'A1' && cell.address != 'A2') {
                cell.border = borderStyles;
            }
            if (cell.value == 'TOTAL') {
                const mergeFrom = `A${startFlag}`;
                const mergeTo = `A${rowNumber - 1}`;
                worksheet.mergeCells(mergeFrom, mergeTo);
                startFlag = rowNumber + 1;
                cell.font = {
                    size: 11.5,
                    bold: true
                };
            }
        });
    });

    return new Promise((resolve, reject) => {
        const fileName = `Report_${fromDate}_${toDate}.xlsx`;
        let filePath = '';
        if (isParner) {
            filePath = `${process.cwd()}/public/xlsx/partner/${fileName}`;
        } else {
            filePath = `${process.cwd()}/public/xlsx/client/${fileName}`;
        }
        workbook.xlsx.writeFile(filePath)
            .then(() => {
                const dataFile = fs.readFileSync(filePath);
                return resolve({
                    data: dataFile,
                    filename: fileName,
                    filepath: filePath,
                    email
                });
            })
            .catch(err => {
                return reject(err);
            });
    });
};
