/* eslint-disable object-property-newline */
const { TABLE, ERROR_CODE, CONTENT_TYPE } = require('../common/enum');
const logger = require('../lib/logger');
const { getMaxChannelId } = require('../lib/utils');
const modelUsers = require('./users');
const modelDefaultMessages = require('./default_messages');
const modelMessages = require('./messages');
const config = require('../lib/helper').getConfig('bbi-chat');
module.exports = (db, cassandra) => {
    return {
        select(returning = [], condition = {}) {
            return new Promise((resolve, reject) => {
                db.select(returning).from(TABLE.NAME.CHANNEL).where(condition)
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`select /channel: ERR: ${error} _ INFO: condition ${JSON.stringify(condition)}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        selectByUserId(bbi_id) {
            return new Promise((resolve, reject) => {
                let lstChannel = [];
                db.select([]).from(TABLE.NAME.CHANNEL)
                    .where({ member1: bbi_id })
                    .orWhere({ member2: bbi_id })
                    .then(result => {
                        lstChannel = result;
                        if (!lstChannel.length || lstChannel.length < 1) return resolve([]);
                        const lstChannelId = lstChannel.map(e => e.channel_id);
                        // lay thong tin user va lastMessages cua channel
                        const lstUserId1 = lstChannel.map(e => {
                            return e.member2;
                        });
                        const lstUserId2 = lstChannel.map(e => {
                            return e.member1;
                        });
                        const lstUserId = lstUserId1.concat(lstUserId2);
                        return Promise.all([
                            modelUsers(db).selectMultiUserBBI(lstUserId),
                            modelMessages(cassandra).getAllByChannel(lstChannelId)
                        ]);
                    })
                    .then(data => {
                        if (!data || data == null) return resolve([]);
                        const lstUsers = data[0];
                        const lstLastMessageChannel = data[1];
                        const dicMessages = {};
                        const dicUsers = {};
                        lstLastMessageChannel.forEach(itemChannelMess => {
                            if (itemChannelMess.rows && itemChannelMess.rows.length > 0) {
                                const itemMess = itemChannelMess.rows[0];
                                const channel_id = itemMess.channel_id || '';
                                if (itemMess.content_type == CONTENT_TYPE.MEDIA) {
                                    const fullPath = config.virtualPath + itemMess.content;
                                    itemMess.content = fullPath;
                                }
                                dicMessages[channel_id] = itemMess;
                            }
                        });
                        for (const user of lstUsers) {
                            dicUsers[user.bbi_id] = user;
                        }
                        const lstChannelFullInfo = lstChannel.map(e => {
                            const obj = Object.assign({}, e);
                            obj.lastMessages = dicMessages[obj.channel_id];
                            if (bbi_id == obj.member1) {
                                // neu user get la member 1 thi lay thong tin member2 tra ve
                                obj.user_id = dicUsers[obj.member2].user_id;
                                obj.fullname = dicUsers[obj.member2].fullname;
                                obj.avatar = dicUsers[obj.member2].avatar;
                            } else {
                                obj.user_id = dicUsers[obj.member1].user_id;
                                obj.fullname = dicUsers[obj.member1].fullname;
                                obj.avatar = dicUsers[obj.member1].avatar;
                            }
                            return obj;
                        });
                        return resolve(lstChannelFullInfo);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectUserByUserId /channel: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },

        selectGroupChannel(userId) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.CHANNEL)
                    .whereIn(TABLE.FIELDS.CHANNEL.MEMBER1, db.select(TABLE.FIELDS.MEMBER_MAPPING.GROUP_ID).from(TABLE.NAME.MEMBER_MAPPING).where({ member_id: userId })) // member 1 luon la id group
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectUserByUserId /channel: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}) {
            let isExist = false;
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.CHANNEL)
                    .where({ member1: body.member1, member2: body.member2 })
                    .orWhere({ member1: body.member2, member2: body.member1 })
                    .then(rows => {
                        if (rows && rows.length > 0) {
                            isExist = true;
                            return resolve(rows[0]);
                        }
                        return getMaxChannelId(db);
                    })
                    .then(maxId => {
                        if (isExist) return resolve();
                        body.channel_id = maxId + 1;
                        return db(TABLE.NAME.CHANNEL).insert(body);
                    })
                    .then(() => {
                        return resolve(body);
                    })
                    .catch(error => {
                        if (error) logger.error(`POST /channel: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        delete(priKey = '') {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.CHANNEL).where({
                    channel_id: priKey
                })
                    .then(rows => {
                        if (rows.length > 0) {
                            return db.delete().from(TABLE.NAME.CHANNEL).where({
                                channel_id: priKey
                            });
                        }
                        logger.log(`delete /channel: ERR: ${ERROR_CODE.NOT_FOUND} _ INFO: id: ${priKey}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`delete /channel: ERR: ${JSON.stringify(error)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        }
    };
};

