/* eslint-disable object-property-newline */
const { TABLE, ERROR_CODE } = require('../common/enum');
// const { bcryptHash, getCurrentTime } = require('../lib/helper');
const logger = require('../lib/logger');

module.exports = db => {
    return {
        select(id) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.DEFAULT_MESSAGES).where({ id })
                    .then(array => {
                        return resolve(array);
                    })
                    .catch(error => {
                        if (error) logger.error(`select default_messages: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        selectByUserId(bbi_id) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.DEFAULT_MESSAGES).where({ bbi_id: bbi_id })
                    .then(array => {
                        return resolve(array);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectUserByUserId default_messages: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        selectMultiUserBBI(lstBBiId = []) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.DEFAULT_MESSAGES)
                    .whereIn(TABLE.FIELDS.USER_INFO.BBI_ID, lstBBiId)
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectMultiUser /user: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.DEFAULT_MESSAGES).insert(body)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`POST default_messages: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.DATA_EXISTS });
                    });
            });
        },
        insertMultilData(arrayData = []) {
            return new Promise((resolve, reject) => {
                db.transaction(trx => {
                    db.raw(db(TABLE.NAME.DEFAULT_MESSAGES).transacting(trx)
                        .insert(arrayData)
                        .toString()
                        .replace('insert', 'INSERT IGNORE'))
                        .then(() => Promise.resolve())
                        .then(trx.commit)
                        .catch(trx.rollback);
                }).then(() => {
                    return resolve(arrayData);
                }).catch(error => {
                    if (error) logger.error(`insertArray default_messages: ERR: ${JSON.stringify(error)}`);
                    return reject({
                        errorCode: ERROR_CODE.UNKNOW_ERROR
                    });
                });
            });
        },
        update(priKey = '', returning = [], body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.DEFAULT_MESSAGES).where({
                    id: priKey
                }).update(body, returning)
                    .then(updatedRows => {
                        if (updatedRows > 0) {
                            resolve({ errorCode: ERROR_CODE.SUCCESS });
                        }
                        logger.log(`update default_messages: ERR: ${ERROR_CODE.DATA_NOT_CHANGED} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.DATA_NOT_CHANGED
                        });
                    })
                    .catch(error => {
                        if (error) logger.error(`update default_messages: ERR: ${JSON.stringify(error)} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        },
        delete(priKey = '') {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.DEFAULT_MESSAGES).where({
                    id: priKey
                })
                    .then(rows => {
                        if (rows.length > 0) {
                            return db.delete().from(TABLE.NAME.DEFAULT_MESSAGES).where({
                                id: priKey
                            });
                        }
                        logger.log(`delete default_messages: ERR: ${ERROR_CODE.NOT_FOUND} _ INFO: id: ${priKey}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`delete default_messages: ERR: ${JSON.stringify(error)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        }
    };
};

