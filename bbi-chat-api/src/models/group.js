/* eslint-disable object-property-newline */
const { TABLE, ERROR_CODE } = require('../common/enum');
// const { bcryptHash, getCurrentTime } = require('../lib/helper');
const { getMaxChannelId, genUUID } = require('../lib/utils');
const { ROLE, CHANNEL_TYPE } = require('../common/enum');
const logger = require('../lib/logger');

module.exports = db => {
    return {
        searchGroup(returning = [], condition = {}) {
            return new Promise((resolve, reject) => {
                db.select(returning).from(TABLE.NAME.GROUP_INFO).where(condition)
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`select /group: ERR: ${error} _ INFO: condition ${JSON.stringify(condition)}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}, lstMember = []) {
            return new Promise(async (resolve, reject) => {
                const group_id = `group${genUUID()}`;
                const groupInfo = {
                    group_id,
                    user_create: body.user_create,
                    group_name: body.group_name,
                    avatar: body.avatar,
                    updated: new Date().getTime()
                };
                const lstMemberMapping = lstMember.map(e => {
                    return {
                        group_id,
                        member_id: e,
                        role: ROLE.USER,
                        updated: new Date().getTime()
                    };
                });
                const member_admin = {
                    group_id,
                    member_id: body.user_create,
                    role: ROLE.ADMIN,
                    updated: new Date().getTime()
                };
                const maxChannelId = await getMaxChannelId(db);
                const new_channel = {
                    channel_id: maxChannelId + 1,
                    member1: group_id,
                    member2: group_id,
                    channel_type: CHANNEL_TYPE.GROUP.toString(),
                    updated: new Date().getTime()
                };
                Promise.all([
                    db(TABLE.NAME.GROUP_INFO).insert(groupInfo),
                    db(TABLE.NAME.MEMBER_MAPPING).insert(member_admin),
                    db(TABLE.NAME.CHANNEL).insert(new_channel),
                    db.transaction(trx => {
                        db.raw(db(TABLE.NAME.MEMBER_MAPPING).transacting(trx)
                            .insert(lstMemberMapping)
                            .toString()
                            .replace('insert', 'INSERT IGNORE'))
                            .then(() => Promise.resolve())
                            .then(trx.commit)
                            .catch(trx.rollback);
                    })
                ])
                    .then(() => {
                        return resolve({ groupInfo, member_admin, lstMemberMapping, new_channel });
                    })
                    .catch(error => {
                        if (error) logger.error(`POST /group: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        update(priKey = '', returning = [], body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.GROUP_INFO).where({
                    group_id: priKey
                }).update(body, returning)
                    .then(updatedRows => {
                        if (updatedRows > 0) {
                            resolve(body);
                        }
                        logger.log(`update /group: ERR: ${ERROR_CODE.DATA_NOT_CHANGED} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.DATA_NOT_CHANGED
                        });
                    })
                    .catch(error => {
                        if (error) logger.error(`update /group: ERR: ${JSON.stringify(error)} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        },
        delete(priKey = '') {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.GROUP_INFO).where({
                    group_id: priKey
                })
                    .then(rows => {
                        if (rows.length > 0) {
                            return db.delete().from(TABLE.NAME.GROUP_INFO).where({
                                group_id: priKey
                            });
                        }
                        logger.log(`delete /group: ERR: ${ERROR_CODE.NOT_FOUND} _ INFO: id: ${priKey}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        const data = {
                            errorCode: ERROR_CODE.SUCCESS
                        };
                        return resolve(data);
                    })
                    .catch(error => {
                        if (error) logger.error(`delete /group: ERR: ${JSON.stringify(error)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        }
    };
};

