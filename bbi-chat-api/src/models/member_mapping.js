/* eslint-disable object-property-newline */
const { TABLE, ERROR_CODE } = require('../common/enum');
// const { bcryptHash, getCurrentTime } = require('../lib/helper');
const { genUUID } = require('../lib/utils');
const { ROLE } = require('../common/enum');
const logger = require('../lib/logger');

module.exports = db => {
    return {
        searchGroup(returning = [], condition = {}) {
            return new Promise((resolve, reject) => {
                db.select(returning).from(TABLE.NAME.MEMBER_MAPPING).where(condition)
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`select /member_mapping: ERR: ${error} _ INFO: condition ${JSON.stringify(condition)}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.MEMBER_MAPPING).insert(body)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`POST /member_mapping: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        update(condition = {}, body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.MEMBER_MAPPING).where(condition).update(body, [])
                    .then(updatedRows => {
                        if (updatedRows > 0) {
                            resolve(body);
                        }
                        logger.log(`update /member_mapping: ERR: ${ERROR_CODE.DATA_NOT_CHANGED} _ INFO: id = ${JSON.stringify(condition)}`);
                        return reject({
                            errorCode: ERROR_CODE.DATA_NOT_CHANGED
                        });
                    })
                    .catch(error => {
                        if (error) logger.error(`update /member_mapping: ERR: ${JSON.stringify(error)} _ INFO: id = ${JSON.stringify(condition)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        },
        delete(condition = {}) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.MEMBER_MAPPING).where(condition)
                    .then(rows => {
                        if (rows.length > 0) {
                            return db.delete().from(TABLE.NAME.MEMBER_MAPPING).where(condition);
                        }
                        logger.log(`delete /member_mapping: ERR: ${ERROR_CODE.NOT_FOUND} _ INFO: id = ${JSON.stringify(condition)}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        const data = {
                            errorCode: ERROR_CODE.SUCCESS
                        };
                        return resolve(data);
                    })
                    .catch(error => {
                        if (error) logger.error(`delete /member_mapping: ERR: ${JSON.stringify(error)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        }
    };
};

