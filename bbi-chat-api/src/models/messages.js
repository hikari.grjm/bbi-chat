const { TABLE, ERROR_CODE } = require('../common/enum');
const { QueryRender } = require('../lib/cassandraQuery');
const logger  = require('../lib/logger');

module.exports = db => {
    return {
        getById(id) {
            return new Promise((resolve, reject) => {
                const queryExec = `SELECT * FROM ${TABLE.NAME.MESSAGES} WHERE ${TABLE.FIELDS.MESSAGES.ID} = ${id} ALLOW FILTERING `;
                db.execute(queryExec)
                    .then(res => {
                        return resolve(res.rows);
                    })
                    .catch(err => {
                        if (err) logger.error(`GET /messages: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        getAllByChannel(lstChannel = []) {
            return new Promise((resolve, reject) => {
                try {
                    const arrayQuery = [];
                    for (const channel_id of lstChannel) {
                        const queryExec = `SELECT * FROM ${TABLE.NAME.MESSAGES} WHERE ${TABLE.FIELDS.MESSAGES.CHANNEL_ID} = ${channel_id} LIMIT 1`;
                        arrayQuery.push(db.execute(queryExec));
                    }
                    Promise.all(arrayQuery)
                        .then(result => {
                            if (result && result.length > 0) return resolve(result);
                        })
                        .catch(err => {
                            if (err) logger.error(`GET /messages: ERR: ${err}`);
                            return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                        });
                } catch (err) {
                    if (err) logger.error(`GET /messages: ERR: ${err}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                }
            });
        },
        loading(channel_id, create_at, limit) {
            return new Promise((resolve, reject) => {
                try {
                    const queryExec = `SELECT * FROM ${TABLE.NAME.MESSAGES} WHERE ${TABLE.FIELDS.MESSAGES.CHANNEL_ID} = ${channel_id} AND create_at < ${create_at} LIMIT ${limit}  ALLOW FILTERING`;
                    db.execute(queryExec)
                        .then(result => {
                            return resolve(result);
                        })
                        .catch(err => {
                            if (err) logger.error(`GET loading /messages: ERR: ${err}`);
                            return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                        });
                } catch (err) {
                    if (err) logger.error(`GET loading /messages: ERR: ${err}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                }
            });
        },
        insert(body = {}) {
            return new Promise((resolve, reject) => {
                const objectInsert = Object.assign({}, body);
                objectInsert.create_at = body.create_at || new Date().getTime();
                objectInsert.updated = new Date().getTime();
                const queryExec = QueryRender.Insert(TABLE.NAME.MESSAGES, objectInsert);
                db.execute(queryExec)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(err => {
                        if (err) logger.error(`POST /messages: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        update(priKey = '', channel_id, body = {}) {
            return new Promise((resolve, reject) => {
                const objUpdate = Object.assign({}, body);
                objUpdate.updated = new Date().getTime();
                const queryExec = QueryRender.Update(TABLE.NAME.MESSAGES, { messages_id: Number(priKey), channel_id }, objUpdate);
                db.execute(queryExec)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(err => {
                        if (err) logger.error(`PUT /messages: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        }
    };
};

