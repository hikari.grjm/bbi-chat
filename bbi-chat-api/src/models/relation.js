/* eslint-disable object-property-newline */
const { TABLE, ERROR_CODE } = require('../common/enum');
// const { bcryptHash, getCurrentTime } = require('../lib/helper');
const logger = require('../lib/logger');

module.exports = db => {
    return {
        selectByUserId(userId) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.RELATION).where({ user_id: userId })
                    .then(array => {
                        return resolve(array);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectUserByUserId /relation: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        selectToUpdate(condition = {}) {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.RELATION)
                    .where(condition)
                    .then(rows => {
                        return resolve(rows);
                    })
                    .catch(error => {
                        if (error) logger.error(`selectToUpdate /relation: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.RELATION).insert(body)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`POST /relation: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.DATA_EXISTS });
                    });
            });
        },
        update(priKey = '', returning = [], body = {}) {
            return new Promise((resolve, reject) => {
                body.updated = new Date().getTime();
                db(TABLE.NAME.RELATION).where({
                    id: priKey
                }).update(body, returning)
                    .then(updatedRows => {
                        if (updatedRows > 0) {
                            resolve({ errorCode: ERROR_CODE.SUCCESS });
                        }
                        logger.log(`update /relation: ERR: ${ERROR_CODE.DATA_NOT_CHANGED} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.DATA_NOT_CHANGED
                        });
                    })
                    .catch(error => {
                        if (error) logger.error(`update /relation: ERR: ${JSON.stringify(error)} _ INFO: id = ${priKey}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        },
        delete(priKey = '') {
            return new Promise((resolve, reject) => {
                db.select([]).from(TABLE.NAME.RELATION).where({
                    id: priKey
                })
                    .then(rows => {
                        if (rows.length > 0) {
                            return db.delete().from(TABLE.NAME.RELATION).where({
                                id: priKey
                            });
                        }
                        logger.log(`delete /relation: ERR: ${ERROR_CODE.NOT_FOUND} _ INFO: id: ${priKey}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(error => {
                        if (error) logger.error(`delete /relation: ERR: ${JSON.stringify(error)}`);
                        return reject({
                            errorCode: ERROR_CODE.UNKNOW_ERROR
                        });
                    });
            });
        }
    };
};

