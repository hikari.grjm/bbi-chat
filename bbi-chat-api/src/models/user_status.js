const { TABLE, ERROR_CODE } = require('../common/enum');
const { QueryRender } = require('../lib/cassandraQuery');
const logger = require('../lib/logger');

module.exports = db => {
    return {
        select(userId) {
            return new Promise((resolve, reject) => {
                const queryExec = `SELECT * FROM ${TABLE.NAME.USER_STATUS} WHERE user_id = ${userId}`;
                db.execute(queryExec)
                    .then(res => {
                        return resolve(res.rows);
                    })
                    .catch(err => {
                        if (err) logger.error(`GET /user_status: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        selectByUserId(lstUserId = []) {
            return new Promise((resolve, reject) => {
                const queryExec = QueryRender.SelectIn(TABLE.NAME.USER_STATUS, 'user_id', lstUserId);
                db.execute(queryExec)
                    .then(res => {
                        return resolve(res.rows);
                    })
                    .catch(err => {
                        if (err) logger.error(`GET /user_status: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        insert(body = {}) {
            return new Promise((resolve, reject) => {
                const objectInsert = Object.assign({}, body);
                objectInsert.updated = new Date().getTime();
                const queryExec = QueryRender.Insert(TABLE.NAME.USER_STATUS, objectInsert);
                db.execute(queryExec)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(err => {
                        if (err) logger.error(`POST /user_status: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        },
        update(priKey = '', body = {}) {
            return new Promise((resolve, reject) => {
                const objUpdate = Object.assign({}, body);
                objUpdate.updated = new Date().getTime();
                const queryExec = QueryRender.Update(TABLE.NAME.USER_STATUS, { user_id: priKey }, objUpdate);
                db.execute(queryExec)
                    .then(() => {
                        return resolve({ errorCode: ERROR_CODE.SUCCESS });
                    })
                    .catch(err => {
                        if (err) logger.error(`PUT /user_status: ERR: ${err}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            });
        }
    };
};

