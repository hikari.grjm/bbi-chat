const { TABLE, ERROR_CODE } = require('../common/enum');
const { bcryptHash, getCurrentTime } = require('../lib/helper');
const { getMaxUserId } = require('../lib/utils');
const { STATUS, OPERATOR, USER_TYPE } = require('../common/enum');
const modelUserStatus = require('./user_status');
const logger = require('../lib/logger');

module.exports = (db, cassandra) => {

    const select = (returning = [], condition = {}) => {
        return new Promise((resolve, reject) => {
            db.select(returning).from(TABLE.NAME.USER_INFO).where(condition)
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`select /user: ERR: ${error} _ INFO: condition ${JSON.stringify(condition)}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };
    const selectUserByUserId = userId => {
        return new Promise((resolve, reject) => {
            select([], { user_id: userId })
                .then(rows => {
                    const userInfo = rows && rows[0] ? rows[0] : null;
                    return resolve(userInfo);
                })
                .catch(error => {
                    if (error) logger.error(`selectUserByUserId /account: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };
    const selectByRelation = lstUserId => {
        return new Promise((resolve, reject) => {
            db.select([]).from(TABLE.NAME.USER_INFO)
                .where(TABLE.FIELDS.USER_INFO.USER_ID, OPERATOR.IsIn, lstUserId)
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`selectUserByUserId /account: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    const selectUserInfoAndUserLoginByUserId = userId => {
        const { USER_LOGIN, USER_INFO } = TABLE.NAME;
        return new Promise((resolve, reject) => {
            db(USER_LOGIN).join(USER_INFO, TABLE.FIELDS.USER_LOGIN.USER_ID, '=', TABLE.FIELDS.USER_INFO.USER_ID)
                .select([TABLE.FIELDS.USER_LOGIN.USER_ID, TABLE.FIELDS.USER_LOGIN.USERNAME, TABLE.FIELDS.USER_INFO.USER_TYPE])
                .where(TABLE.FIELDS.USER_INFO.USER_ID, userId)
                .then(rows => {
                    const userInfo = rows && rows[0] ? rows[0] : null;
                    return resolve(userInfo);
                })
                .catch(error => {
                    if (error) logger.error(`selectUserInfoAndUserLoginByUserId /account: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    const selectAllUsers = () => {
        return new Promise((resolve, reject) => {
            select([], {})
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`selectAllUsers /user: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };
    const selectMultiUser = (lstUserId = []) => {
        return new Promise((resolve, reject) => {
            db.select([]).from(TABLE.NAME.USER_INFO)
            .whereIn(TABLE.FIELDS.USER_INFO.USER_ID, lstUserId)
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`selectMultiUser /user: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };
    const selectMultiUserBBI = (lstBBiId = []) => {
        return new Promise((resolve, reject) => {
            db.select([]).from(TABLE.NAME.USER_INFO)
            .whereIn(TABLE.FIELDS.USER_INFO.BBI_ID, lstBBiId)
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`selectMultiUser /user: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    const searchUser = _param => {
        const { USER_INFO } = TABLE.NAME;
        const { USER_ID, FULLNAME } = TABLE.FIELDS.USER_INFO;
        const param = typeof _param == 'string' ? _param.toLowerCase() : '';
        return new Promise((resolve, reject) => {
            db(USER_INFO)
                .where(qb => {
                    if (param === STATUS.ACTIVE || param === STATUS.INACTIVE) {
                        qb.where({ status: param });
                    } else {
                        qb.orWhere(USER_ID, 'like', `%${param}%`).orWhere(FULLNAME, 'like', `%${param}%`);
                    }
                })
                .then(rows => {
                    return resolve(rows);
                })
                .catch(error => {
                    if (error) logger.error(`selectUserInfoAndUserLoginByUserId /account: ERR: ${error}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    const insert = (body = {}) => {
        return new Promise(async (resolve, reject) => {
            try {
                const { username, password, fullname, address, avatar, gender } = body;
                const maxUserId = await getMaxUserId(db);
                const objUserLogin = {
                    user_id: maxUserId + 1,
                    username,
                    password: bcryptHash(password),
                    created: getCurrentTime()
                };
                const objUserInfo = {
                    user_id: maxUserId + 1,
                    fullname,
                    address,
                    avatar,
                    gender,
                    user_type: USER_TYPE.NORMAL,
                    status: STATUS.ACTIVE
                };
                const userStatus = {
                    user_id: maxUserId + 1,
                    status: 0
                };
                db.select([TABLE.FIELDS.USER_LOGIN.USER_ID]).from(TABLE.NAME.USER_LOGIN).where({ username: body.username })
                    .then(rows => {
                        if (!rows.length) {
                            return Promise.all([
                                db(TABLE.NAME.USER_LOGIN).insert(objUserLogin),
                                db(TABLE.NAME.USER_INFO).insert(objUserInfo),
                                modelUserStatus(cassandra).insert(userStatus)
                            ]);
                        }
                        logger.error(`POST /user: ERR: ${ERROR_CODE.DATA_EXISTS} _ INFO: ${username}`);
                        return Promise.reject();
                    })
                    .then(() => {
                        return resolve(objUserInfo);
                    })
                    .catch(error => {
                        if (error) logger.error(`POST /user: ERR: ${error}`);
                        return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                    });
            } catch (err) {
                logger.error(`POST /user: ERR: ${ERROR_CODE.DATABASE_ERROR} _ INFO: ${JSON.stringify(err)}`);
                return reject({ errorCode: ERROR_CODE.DATABASE_ERROR });
            }
        });
    };

    const updatePassword = (priKey = '', returning = [], body = {}) => {
        return new Promise((resolve, reject) => {
            const password = bcryptHash(body.password);
            db(TABLE.NAME.USER_LOGIN).where({ user_id: priKey }).update({ password }, returning)
                .then(() => {
                    return resolve(null);
                })
                .catch(error => {
                    if (error) logger.error(`updatePassword /user: ERR: ${error} _ INFO: user_id = ${priKey}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    const updateUserInfo = (priKey = '', returning = [], body = {}) => {
        return new Promise((resolve, reject) => {
            db(TABLE.NAME.USER_INFO).where({ user_id: priKey }).update(body, returning)
                .then(updatedRows => {
                    if (updatedRows > 0) {
                        return returning.length > 0 ? resolve(returning) : resolve(null);
                    }
                    logger.error(`updateUserInfo /user: ERR: ${ERROR_CODE.DATA_NOT_CHANGED} _ INFO: user_id = ${priKey}`);
                    return resolve({ errorCode: ERROR_CODE.DATA_NOT_CHANGED });
                })
                .catch(error => {
                    if (error) logger.error(`updateUserInfo /user: ERR: ${error} _ INFO: user_id = ${priKey}`);
                    return reject({ errorCode: ERROR_CODE.UNKNOW_ERROR });
                });
        });
    };

    return {
        select,
        selectUserByUserId,
        selectUserInfoAndUserLoginByUserId,
        selectAllUsers,
        searchUser,
        insert,
        updatePassword,
        updateUserInfo,
        selectByRelation,
        selectMultiUser,
        selectMultiUserBBI
    };
};

