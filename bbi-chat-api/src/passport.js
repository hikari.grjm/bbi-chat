const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const ExtractJWT = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;
const { bcryptCompare, clone, bcryptHash } = require('./lib/helper');
const logger = require('./lib/logger');
const { TABLE, ERROR_CODE,USER_TYPE } = require('./common/enum');

// eslint-disable-next-line no-unused-vars
module.exports = (config, db, cassandra) => {
    // Using LocalStrategy for /auth step
    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, (username, password, done) => {
        const { USER_LOGIN } = TABLE.NAME;
        db.from(USER_LOGIN).where({ username })
            .select('*')
            .then(async rows => {
                // neu khong thi lam binh thuong
                const dbUserInfo = rows[0];
                if (!dbUserInfo) return done(null, null, ERROR_CODE.UNKNOW_ERROR);
                const isSamePassword = bcryptCompare(password, dbUserInfo.password);
                if (!isSamePassword) {
                    return done(null, null, ERROR_CODE.UNKNOW_ERROR);
                }
                const attachment = clone(dbUserInfo);
                Reflect.deleteProperty(attachment, 'password');
                return done(null, attachment, ERROR_CODE.SUCCESS);
            })
            .catch(error => {
                return done(error, null, ERROR_CODE.UNKNOW_ERROR);
            });
    }));

    // Using JWTStrategy to authenticate access for all apis, except /auth
    passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.secretKey
    }, (jwtPayload, done) => {
        const { USER_INFO } = TABLE.NAME;
        if (jwtPayload.username == 'admin') {
            const attachment = clone(jwtPayload);
            attachment.user_type = USER_TYPE.ADMIN;
            return done(null, attachment, ERROR_CODE.SUCCESS);
        }
        db.select([])
            .from(USER_INFO)
            .where({ bbi_id: jwtPayload.bbi_id })
            .then(async rows => {
                if (!rows.length) {
                    return done(null, null, ERROR_CODE.UNKNOW_ERROR);
                }
                const dbUserInfo = rows[0];
                const isPass = dbUserInfo.bbi_id === jwtPayload.bbi_id;
                if (isPass) {
                    const attachment = clone(dbUserInfo);
                    attachment.role = dbUserInfo.user_type;
                    return done(null, attachment, ERROR_CODE.SUCCESS); // 'dbUserInfo' is an attachment for all apis. Can be use this for check role ^.^
                }
                return done(null, null, ERROR_CODE.UNKNOW_ERROR);

            })
            .catch(err => {
                return done(err, null, ERROR_CODE.UNKNOW_ERROR);
            });
    }));

    return passport;
};