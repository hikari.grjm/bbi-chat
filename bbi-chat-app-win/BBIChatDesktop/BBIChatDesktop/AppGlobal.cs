﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BBIChatDesktop
{
    public class AppGlobal
    {
        private static readonly string _appApi = @"http://chat.bbivietnam.vn/api/";
        public static string Token { get; set; }

        public static string GetLinkAuth()
        {
            return _appApi + "auth";
        }
    }
}
