﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BBIChatDesktop.Req;
using BBIChatDesktop.Res;
using Newtonsoft.Json;

namespace BBIChatDesktop.Form
{
    public partial class FrmLogin : UserControl
    {
        public EventHandler<EventArgs> OnClientFinishLogin;
        public FrmLogin()
        {
            InitializeComponent();
            lblErrorCode.Text = "";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorCode.Text = "";
                using (var client = new HttpClient())
                {
                    string path = AppGlobal.GetLinkAuth();
                    var authObj = new AuthReq
                    {
                        username = txtUserName.Text,
                        password = txtPassword.Text
                    };
                    var myContent = JsonConvert.SerializeObject(authObj);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    var responseTask = client.PostAsync(path, byteContent);
                    responseTask.Wait();
                    var result = responseTask.Result;
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var resPos = JsonConvert.DeserializeObject<AuthRes>(readTask.Result);
                        if (string.IsNullOrEmpty(resPos.token))
                        {
                            lblErrorCode.Text = @"Login fail with empty token";
                            return;
                        }

                        //Luu lai thong tin token
                        AppGlobal.Token = resPos.token;
                        //Dong form de load thong tin 
                        if (OnClientFinishLogin != null)
                            OnClientFinishLogin(this, new EventArgs());
                    }
                    else
                    {
                        //    LogTo.Info("result.StatusCode = " + result.StatusCode);
                        //    LogTo.Info("req error : " + JsonConvert.SerializeObject(req));
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        var resPos = JsonConvert.DeserializeObject<AuthRes>(readTask.Result);
                        lblErrorCode.Text = resPos.errorCode;
                    }
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
