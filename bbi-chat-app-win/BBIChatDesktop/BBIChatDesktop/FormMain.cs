﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Anotar.NLog;
using BBIChatDesktop.Form;

namespace BBIChatDesktop
{
    public partial class FormMain : System.Windows.Forms.Form
    {
        private FrmLogin _frmLogin = null;
        public FormMain()
        {
            InitializeComponent();

            LogTo.Info("Press login");
            //Add form login
            splitContainer1.Visible = false;
            
            _frmLogin = new FrmLogin();
            _frmLogin.Visible = true;
            _frmLogin.OnClientFinishLogin += ProcessLoginSuccess;


            this.Controls.Add(_frmLogin);
        }

        private void ProcessLoginSuccess(object sender, EventArgs e)
        {
            try
            {
                //Khi login thanh cong
                //Dong form dang nhap
                _frmLogin.Visible = false;
                splitContainer1.Visible = true;
                //Mo chi tiet thong tin main
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
