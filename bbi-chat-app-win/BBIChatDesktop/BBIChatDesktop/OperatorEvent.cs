﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBIChatDesktop
{
    public class OperatorEvent
    {
        #region singleton implement

        public static OperatorEvent Instance
        {
            get { return Nested.Instance; }
        }

        class Nested
        {
            static Nested() { }
            internal static readonly OperatorEvent Instance = new OperatorEvent();
        }
        #endregion

        public void Init()
        {
            
        }

        #region Event Realtime
        public EventHandler<EventArgs> OnChatMessageChanged;
        #endregion

        #region Event Response
        public EventHandler<EventArgs> OnGroupChatResponse;
        #endregion
    }
}
