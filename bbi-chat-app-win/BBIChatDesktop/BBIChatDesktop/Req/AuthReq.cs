﻿namespace BBIChatDesktop.Req
{
    public class AuthReq
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
