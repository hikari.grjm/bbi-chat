﻿namespace BBIChatDesktop.Res
{
    public class AuthRes
    {
        public string token { get; set; }
        public string errorCode { get; set; }
    }
}
