module.exports = {
    COMMON: {
        CONNECTION: 'connection',
        CONNECTED: 'connected',
        DISCONNECT: 'disconnect',
        RECONNECT: 'reconnect',
        ERROR: 'error',
        PING: 'ping',
        PONG: 'pong',
        PACKET: 'packet',
        PACKET_CREATE: 'packetCreate'
    },
    REQUEST: {
        SERVER: {
            USER_INFO: 'req#server#user_info',
            USER_STATUS: 'req#server#user_status',
            JOIN_ROOM: 'req#server#join_room',
            CHAT_TO_SOMEONE: 'req#server#chat_to_someone',
            LIST_USER_STATUS: 'req#server#list_user_status'
        },
        ALL: {
            USER_STATUS: 'req#all#user_status'
        },
        CHANNEL: {
            USER_STATUS: 'req#channel#user_status',
            MESSAGE_STATUS: 'req#channel#message_status',
            SEND_MESSAGE: 'req#channel#send_message',
            READ: 'req#channel#read',
            TYPING: 'req#channel#typing',
            UNTYPING: 'req#channel#untyping'
        }
    },
    RESPONSE: {
        ALL: {
            USER_STATUS: 'res#all#user_status'
        },
        CHANNEL: {
            USER_STATUS: 'res#channel#user_status',
            SEND_MESSAGE: 'res#channel#send_message',
            MESSAGE_STATUS: 'res#channel#message_status',
            TYPING: 'res#channel#typing',
            UNTYPING: 'res#channel#untyping'
        },
        SPECIFIC: {
            JOIN_ROOM: 'res#specific#join_room',
            LIST_USER_STATUS: 'res#specific#list_user_status'
        }
    },
    ERROR: {
        SPECIFIC: {
            MESSAGE_STATUS: 'error#specific#message_status',
            SEND_MESSAGE: 'error#specific#send_message'
        }
    }
};