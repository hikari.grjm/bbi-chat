const baseUrlAPI = 'http://localhost:8090';

module.exports = {
    URL: {
        login: `${baseUrlAPI}/api/auth`,
        relation: `${baseUrlAPI}/api/relation`,
        refresh: `${baseUrlAPI}/api/refresh`,
        chanel: `${baseUrlAPI}/api/chanel`,
        messages: `${baseUrlAPI}/api/messages`,
        user_status: `${baseUrlAPI}/api/user_status`
    },
    USER_STATUS: {
        offline: 0,
        online: 1
    },
    MESSAGE_STATUS: {
        200: 'success',
        400: 'failure',
        701: 'seen'
    },
    REASON: {
        0: 'client namespace disconnect'
    }
};