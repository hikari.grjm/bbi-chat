module.exports = {
    INVALID_USER: {
        errorCode: 'INVALID_USER'
    },
    INVALID_MESSAGE: {
        errorCode: 'INVALID_MESSAGE'
    },
    UPDATE_FAIL: {
        errorCode: 'UPDATE_FAIL'
    }
};