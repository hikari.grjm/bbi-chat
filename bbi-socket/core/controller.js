// const actionValue = require('../common/action');
const logger = require('../utils/logger');
const handler = require('./handler');

module.exports = (io, socket) => {
    return {
        getDicRoomByClientId: clientId => {
            if (io && io.sockets &&
                io.sockets.adapter &&
                io.sockets.adapter.sids &&
                io.sockets.adapter.sids[clientId]
            ) {
                return io.sockets.adapter.sids[clientId];
            }
            return {};
        },
        getDicSocketsByRoomName: roomName => {
            if (io && io.sockets &&
                io.sockets.adapter &&
                io.sockets.adapter.rooms &&
                io.sockets.adapter.rooms[roomName]
            ) {
                return io.sockets.adapter.rooms[roomName].sockets;
            }
            return {};
        },
        getSocketInfoBySocketId: socketId => {
            if (io && io.sockets &&
                io.sockets.connected &&
                io.sockets.connected[socketId]
            ) {
                return io.sockets.connected[socketId];
            }
            return null;
        },
        sender: {
            // sending to the client
            sendMsgToOne: (action, data) => {
                return socket.emit(action, data);
            },
            // sending to all clients except sender
            sendMsgToAllExceptSender: (action, data) => {
                return socket.broadcast.emit(action, data);
            },
            // sending to all clients in room except sender
            sendMsgToRoomExceptSender: (roomName, action, data) => {
                return socket.to(roomName).emit(action, data);
            },
            // sending to all clients in room, including sender
            sendMsgToRoom: (roomName, action, data) => {
                return io.in(roomName).emit(action, data);
            },
            // sending to all clients in 2 room, including sender
            sendMsgToTwoRoom: (room1, room2, action, data) => {
                return io.in(room1).in(room2)
                    .emit(action, data);
            },
            // sending to all clients in namespace, including sender
            sendMsgToNamespace: (namespace, action, data) => {
                return io.of(namespace).emit(action, data);
            },
            // sending to a specific room in a specific namespace, including sender
            sendMsgToRoomInNamespace: (namespace, room, action, data) => {
                return io.of(namespace).to(room)
                    .emit(action, data);
            },
            // sending with acknowledgement
            sendMsgWithQuestion: (action, question, cb) => {
                return socket.emit(action, question, cb);
            },
            // sending without compression
            sendMsgWithoutCompression: (action, data) => {
                return socket.compress(false).emit(action, data);
            },
            // sending a message that might be dropped if the client is not ready to receive messages
            sendMsgVolatilization: (action, data) => {
                return socket.volatile.emit(action, data);
            },
            // specifying whether the data to send has binary data
            sendMsgBinary: (action, data) => {
                return socket.binary(false).emit(action, data);
            },
            // sending to all clients on this node (when using multiple nodes)
            sendMsgToNode: (action, data) => {
                return io.local.emit(action, data);
            },
            // sending to all connected clients
            sendMsgToAll: (action, data) => {
                return io.emit(action, data);
            },
            sendMsgError: (action, errorCode) => {
                return socket.emit(action, errorCode);
            },

            sendMsgToJoinRoom: (roomName, cb) => {
                return socket.join(roomName, () => {
                    const userInfo = handler.getDicUsersOnlineBySocketId(socket.id);
                    logger.info(`sendMsgToJoinRoom _ [${socket.id}][${userInfo.username}] join room <${roomName}> successfully.`);
                    if (cb) return cb();
                });
            },
            sendMsgToJoinRoomBySocketInfo: (socketInfo, roomName, cb) => {
                if (!socketInfo) {
                    return logger.info(`sendMsgToJoinRoomBySocketInfo _ socketInfo is NULL`);
                }
                return socketInfo.join(roomName, () => {
                    const userInfo = handler.getDicUsersOnlineBySocketId(socketInfo.id);
                    logger.info(`sendMsgToJoinRoomBySocketInfo _ [${socket.id}][${userInfo.username}] join room <${roomName}> successfully.`);
                    if (cb) return cb();
                });
            }
        },
        receiver: {
            receiveMsgRequest: (actions, cb) => {
                return socket.on(actions, data => {
                    if (data != null) {
                        return cb(data);
                    }
                    return cb(null);
                });
            },
            receiveMsgResponse: (actions, cb) => {
                return socket.on(actions, data => {
                    if (data != null) {
                        return cb({
                            data,
                            statusCode: 200
                        });
                    }
                    return cb({
                        data: null,
                        statusCode: 400
                    });
                });
            }
        }
    };
};