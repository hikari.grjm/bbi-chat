const dicRooms = {};
const dicUsersOnline = {};

module.exports = {
    addUserToDicUsersOnline: (socketId, userInfo, cb) => {
        if (!socketId || !userInfo || socketId == '') return cb ? cb(false) : false;
        const key = socketId;
        if (!Reflect.has(dicUsersOnline, key)) {
            dicUsersOnline[key] = userInfo;
            return cb ? cb(true) : true;
        }
        return cb ? cb(false) : false;
    },

    removeUserFromDicUsersOnline: (socketId, cb) => {
        if (!socketId || socketId == '') return cb ? cb(false) : false;
        const key = socketId;
        if (Reflect.has(dicUsersOnline, key)) {
            delete dicUsersOnline[key];
            return cb ? cb(true) : true;
        }
        return cb ? cb(false) : false;
    },

    getDicUsersOnline: () => {
        return dicUsersOnline;
    },

    getDicUsersOnlineBySocketId: socketId => {
        const key = socketId;
        if (Reflect.has(dicUsersOnline, key)) {
            return dicUsersOnline[key];
        }
        return {
            user_id: 0,
            username: 'Anonymous'
        };
    },

    updateDicRoomsBySocketId: (socketId, dicCurrentRoom = null, cb) => {
        if (!socketId) return cb ? cb(false) : false;
        const key = socketId;
        if (dicCurrentRoom == null) {
            delete dicRooms[key];
        }
        dicRooms[key] = JSON.parse(JSON.stringify(dicCurrentRoom));
        return cb ? cb(true) : true;
    },

    getDicRoomsBySocketId: socketId => {
        if (!socketId) return {};
        const key = socketId;
        if (Reflect.has(dicRooms, key) && dicRooms[key] != null) {
            return dicRooms[key];
        }
        return {};
    },

    addRoomToDicRooms: (roomName, cb) => {
        if (!roomName) return cb ? cb(false) : false;
        const key = roomName;
        if (!Reflect.has(dicRooms, key)) {
            dicRooms[key] = roomName;
            return cb ? cb(true) : true;
        }
        return cb ? cb(false) : false;
    },

    removeRoomFromDicRooms: (roomName, cb) => {
        if (!roomName) return cb ? cb(false) : false;
        const key = roomName;
        if (!Reflect.has(dicRooms, key)) {
            delete dicRooms[key];
            return cb ? cb(true) : true;
        }
        return cb ? cb(false) : false;
    }
};