/* eslint-disable guard-for-in */
const handler = require('../handler');
const logger = require('../../utils/logger');
const converter = require('../../utils/converter');

module.exports = (controller, client, data) => {
    try {
        if (data && data.channel_id) {
            const channelId = data.channel_id;
            const roomName = channelId;
            // A chat to B.
            controller.sender.sendMsgToJoinRoom(roomName, () => {
                const dicRooms = controller.getDicRoomByClientId(client.id);
                handler.updateDicRoomsBySocketId(client.id, dicRooms, () => {
                    const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
                    logger.info(`updateDicRoomsBySocketId _ [${client.id}][${userInfo.username}] _ DicRooms: ${converter.toString(dicRooms)}`);
                });
            });

            if (data.object_id) {
                // 'receiverId' is default channel name of each socket when socket connected. See more: userInfo.js
                const receiverId = data.object_id;
                const dicSockets = controller.getDicSocketsByRoomName(receiverId);

                for (const socketId in dicSockets) {
                    const socketInfo = controller.getSocketInfoBySocketId(socketId);
                    controller.sender.sendMsgToJoinRoomBySocketInfo(socketInfo, roomName, () => {
                        const dicRooms = controller.getDicRoomByClientId(socketInfo.id);
                        handler.updateDicRoomsBySocketId(socketInfo.id, dicRooms, () => {
                            const userInfo = handler.getDicUsersOnlineBySocketId(socketInfo.id);
                            logger.info(`updateDicRoomsBySocketId _ [${socketInfo.id}][${userInfo.username}] _ DicRooms: ${converter.toString(dicRooms)}`);
                        });
                    });
                }
            }
        } else {
            logger.error(`CHANNEL_ID or Object_id is undefined or NULL`);
        }
    } catch (error) {
        logger.error(`Exception in ChatToSomeOne: ${converter.toString(error)}`);
    }
};