/* eslint-disable guard-for-in */
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const { USER_STATUS } = require('../../common/constant');
const handler = require('../handler');

module.exports = (controller, client, reason) => {
    try {
        const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
        logger.info(`User disconnected: [${client.id}][${userInfo.username}] with reason: ${reason.data}`);
        handler.removeUserFromDicUsersOnline(client.id, isDelete => {
            if (isDelete) {
                logger.info(`Deleted clientId [${client.id}][${userInfo.username}] from dicUserOnline sucessfully.`);

                if (!userInfo.user_id || userInfo.user_id === 0) {
                    logger.sendToTelegram(`DISCONNECT: Invalid UserInfo _ ClientId: ${client.id} _ ${converter.toString(client.handshake.headers)}`);
                } else {
                    logger.info(`DISCONNECT: SUCCESS _ ${converter.toString(userInfo)} _ ${converter.toString(client.handshake.headers)}`);
                    httpz.updateUserStatus(userInfo.user_id, { status: USER_STATUS.offline }); // update status of user and not receive reponse
                }
                const objData = {
                    user_id: userInfo.user_id,
                    status: USER_STATUS.offline
                };
                // SendAll
                controller.sender.sendMsgToAll(action.RESPONSE.ALL.USER_STATUS, objData);
                // Send to room except

                const dicRooms = handler.getDicRoomsBySocketId(client.id);
                for (const room in dicRooms) {
                    controller.sender.sendMsgToRoomExceptSender(room, action.RESPONSE.CHANNEL.USER_STATUS, objData);
                }

                // dicCurrentRoom = null -> remove from dictionary, free memory
                handler.updateDicRoomsBySocketId(client.id, null, () => {
                    logger.sendToTelegram(`Clear memory _ [${client.id}][${userInfo.username}] _ DicRooms: ${converter.toString(dicRooms)} _ Reason: ${reason.data}`);
                });
            }
        });
    } catch (error) {
        logger.error(`Exception in Disconnect: ${converter.toString(error)}`);
    }
};