module.exports = {
    joinRoom: require('./joinRoom'),
    sendMessage: require('./sendMessage'),
    chatToSomeOne: require('./chatToSomeOne'),
    typing: require('./typing'),
    untyping: require('./untying'),
    read: require('./read'),
    userInfo: require('./userInfo'),
    disconnect: require('./disconnect'),
    userStatus: require('./userStatus'),
    listUserStatus: require('./listUserStatus')
};