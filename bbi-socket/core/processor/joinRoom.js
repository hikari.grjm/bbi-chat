const logger = require('../../utils/logger');
const converter = require('../../utils/converter');
const handler = require('../handler');
const action = require('../../common/action');

module.exports = (controller, client, channelIdOrListChannelId) => {
    try {
        if (channelIdOrListChannelId &&
            (typeof channelIdOrListChannelId == 'string' || Array.isArray(channelIdOrListChannelId))
        ) {
            controller.sender.sendMsgToJoinRoom(channelIdOrListChannelId, () => {
                const dicRooms = controller.getDicRoomByClientId(client.id);
                handler.updateDicRoomsBySocketId(client.id, dicRooms, () => {
                    const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
                    logger.info(`updateDicRoomsBySocketId _ [${client.id}][${userInfo.username}] _ DicRooms: ${converter.toString(dicRooms)}`);
                    controller.sender.sendMsgError(action.RESPONSE.SPECIFIC.JOIN_ROOM, null);
                });
            });
        } else {
            logger.error(`channelIdOrListChannelId is INVALID _ ${channelIdOrListChannelId}`);
        }
    } catch (error) {
        logger.error(`Exception in JoinRoom: ${converter.toString(error)}`);
    }
};