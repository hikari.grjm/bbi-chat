/* eslint-disable guard-for-in */
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const handler = require('../handler');

module.exports = async (controller, client, lstUserId) => {
    try {
        if (lstUserId && Array.isArray(lstUserId)) {
            logger.info(`listUserStatus: ${converter.toString(lstUserId)}`);
            const lstUserStatus = await httpz.getListUserStatus(lstUserId);
            const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
            logger.info(`${action.RESPONSE.SPECIFIC.LIST_USER_STATUS} _ [${client.id}][${userInfo.username}] _ ${converter.toString(lstUserStatus)}`);
            controller.sender.sendMsgError(action.RESPONSE.SPECIFIC.LIST_USER_STATUS, lstUserStatus);
        } else {
            logger.error(`lstUserId is INVALID _ ${lstUserId}`);
        }
    } catch (error) {
        logger.error(`Exception in ListUserInfo: ${converter.toString(error)}`);
    }
};