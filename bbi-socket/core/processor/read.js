const errorCode = require('../../common/errorCode');
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const { MESSAGE_STATUS } = require('../../common/constant');

module.exports = (controller, client, data) => {
    try {
        const objData = {
            channel_id: data.channel_id,
            message_id: data.message_id,
            user_id: data.user_id,
            updated: new Date().getTime(),
            status: MESSAGE_STATUS[200]
        };
        return httpz.updateMessageStatus(objData.message_id, { status: MESSAGE_STATUS[701] })
            .then(isUpdate => {
                if (isUpdate) {
                    objData.status = MESSAGE_STATUS[701];
                    logger.info(`updateMessageStatus _ SUCCESS _ ${JSON.stringify(objData)}`);
                    controller.sender.sendMsgToRoom(objData.channel_id, action.RESPONSE.CHANNEL.MESSAGE_STATUS, objData);
                } else {
                    logger.info(`updateMessageStatus _ FAILURE _ ${JSON.stringify(objData)}`);
                    controller.sender.sendMsgError(action.ERROR.SPECIFIC.MESSAGE_STATUS, errorCode.UPDATE_FAIL);
                }
            });
    } catch (error) {
        logger.error(`Exception in Read: ${converter.toString(error)}`);
    }
};