const errorCode = require('../../common/errorCode');
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const { getConfig } = require('../../utils/helper');
const Filter = require('bad-words');
const badwords = getConfig('badwords').badwords;
const filter = new Filter({ list: badwords });

module.exports = (controller, client, data) => {
    try {
        if (!data || (data && (!data.sender_id || !data.channel_id))) {
            controller.sender.sendMsgError(action.REQUEST.CHANNEL.SEND_MESSAGE, errorCode.INVALID_MESSAGE);
            logger.error(`sendMessage _ INVALID_MESSAGE: ${converter.toString(data)}`);
            return;
        }

        // Save message to database by API and emit message back to client again
        logger.info(`sendMessage _ ${action.REQUEST.CHANNEL.SEND_MESSAGE} _ ${converter.toString(data)}`);

        const messageModel = {};
        messageModel.channel_id = data.channel_id;
        messageModel.sender_id = data.sender_id;
        messageModel.receiver_id = data.receiver_id;
        messageModel.content = filter.clean(data.content);
        messageModel.content_type = data.content_type;
        messageModel.time = new Date().getTime();

        const roomName = data.channel_id;

        const objMessage = {
            content: messageModel.content,
            content_type: messageModel.content_type,
            sender_id: messageModel.sender_id,
            channel_id: messageModel.channel_id
        };

        httpz.createMessage(objMessage)
            .then(res => {
                if (res) {
                    // A chat to B
                    controller.sender.sendMsgToRoom(roomName, action.RESPONSE.CHANNEL.SEND_MESSAGE, messageModel);
                    logger.info(`httpz.createMessage _ SUCCESS _ CHANNEL_ID: ${objMessage.channel_id} _ ${JSON.stringify(objMessage)}`);
                } else {
                    logger.error(`httpz.createMessage _ FAILURE _ ${JSON.stringify(objMessage)}`);
                    controller.sender.sendMsgError(action.REQUEST.CHANNEL.SEND_MESSAGE, errorCode.INVALID_MESSAGE);
                }
            });
    } catch (error) {
        logger.error(`Exception in SendMessage: ${converter.toString(error)}`);
    }
};