const action = require('../../common/action');
const logger = require('../../utils/logger');
const converter = require('../../utils/converter');

module.exports = (controller, data) => {
    try {
        if (!data) {
            return;
        }
        const roomName = data.channel_id;
        // logger.info(`TYPING: CHANNEL_ID: ${data.channel_id} _ SENDER_ID: ${data.sender_id}`);
        controller.sender.sendMsgToRoom(roomName, action.RESPONSE.CHANNEL.TYPING, {
            channel_id: data.channel_id,
            sender_id: data.sender_id
        });
    } catch (error) {
        logger.error(`Exception in Typing: ${converter.toString(error)}`);
    }
};