/* eslint-disable guard-for-in */
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const { USER_STATUS } = require('../../common/constant');
const handler = require('../handler');

module.exports = (controller, client, response) => {
    try {
        const userInfo = response.data;
        if (!userInfo.user_id || userInfo.user_id === 0) {
            logger.sendToTelegram(`UserInfo: Invalid UserInfo _ ${converter.toString(client.handshake.headers)}`);
        } else {
            logger.info(`UserInfo: SUCCESS _ ${converter.toString(userInfo)} _ ${converter.toString(client.handshake.headers)}`);
        }
        handler.addUserToDicUsersOnline(client.id, userInfo, isUpdated => {
            if (isUpdated) {
                logger.info(`Updated dicUserOnline: ${JSON.stringify(handler.getDicUsersOnlineBySocketId(client.id))}`);
                httpz.updateUserStatus(userInfo.user_id, { status: USER_STATUS.online }); // update status of user and not receive reponse
                const objData = {
                    user_id: userInfo.user_id,
                    status: USER_STATUS.online
                };
                // By default, when user emit UserInfo, auto create room has name is user's user_id
                controller.sender.sendMsgToJoinRoom(objData.user_id);
                logger.info(`Create default channel with channel_name is <${objData.user_id}>`);

                // SendAll
                controller.sender.sendMsgToAll(action.RESPONSE.ALL.USER_STATUS, objData);

                // Send to room except
                const dicRooms = handler.getDicRoomsBySocketId(client.id);
                logger.info(`UserInfo _ DicRoom: ${converter.toString(dicRooms)}`);
                for (const room in dicRooms) {
                    controller.sender.sendMsgToRoomExceptSender(room, action.RESPONSE.CHANNEL.USER_STATUS, objData);
                }
                handler.updateDicRoomsBySocketId(client.id, dicRooms, () => {
                    logger.info(`updateDicRoomsBySocketId _ [${client.id}][${userInfo.username}] _ DicRooms: ${converter.toString(dicRooms)}`);
                });
            }
        });
    } catch (error) {
        logger.error(`Exception in UserInfo: ${converter.toString(error)}`);
    }
};