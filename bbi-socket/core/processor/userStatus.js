/* eslint-disable guard-for-in */
const logger = require('../../utils/logger');
const httpz = require('../../utils/http');
const converter = require('../../utils/converter');
const action = require('../../common/action');
const { USER_STATUS } = require('../../common/constant');
const handler = require('../handler');

module.exports = (controller, client, data) => {
    try {
        const userInfo = handler.getDicUsersOnlineBySocketId(client.id);

        if (userInfo.user_id === 0 || userInfo.user_id != data.user_id) {
            userInfo.user_id = data.user_id;
        }

        if (!userInfo.user_id || userInfo.user_id === 0) {
            logger.sendToTelegram(`UserStatus: Invalid UserInfo data: ${converter.toString(userInfo)} _ ${converter.toString(client.handshake.headers)}`);
        } else {
            logger.info(`UserStatus: SUCCESS _ ${converter.toString(userInfo)} _ ${converter.toString(client.handshake.headers)}`);
        }

        userInfo.status = data.status == USER_STATUS.online
            ? userInfo.status = USER_STATUS.online
            : userInfo.status = USER_STATUS.offline;

        httpz.updateUserStatus(userInfo.user_id, { status: userInfo.status }); // update status of user and not receive reponse
        const objData = {
            user_id: userInfo.user_id,
            status: userInfo.status
        };
        // SendAll
        controller.sender.sendMsgToAll(action.RESPONSE.ALL.USER_STATUS, objData);

        // Send to room except
        const dicRooms = handler.getDicRoomsBySocketId(client.id);
        logger.info(`UserStatus _ DicRoom: ${converter.toString(dicRooms)}`);
        for (const room in dicRooms) {
            controller.sender.sendMsgToRoomExceptSender(room, action.RESPONSE.CHANNEL.USER_STATUS, objData);
        }
    } catch (error) {
        logger.error(`Exception in UserInfo: ${converter.toString(error)}`);
    }
};