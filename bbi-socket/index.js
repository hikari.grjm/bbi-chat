/* eslint-disable guard-for-in */
const action = require('./common/action');
const logger = require('./utils/logger');
const { getConfig } = require('./utils/helper');
// const errorCode = require('./common/errorCode');
const config = getConfig('bbi-socket');
// const handler = require('./core/handler');
const processor = require('./core/processor');
const port = config && config.port ? config.port : 3000;
const converter = require('./utils/converter');

const app = require('express')();
const server = app.listen(port, () => logger.info(`Socket server is running on port ${port}`));
const io = require('socket.io').listen(server, {
    pingInterval: 2000,
    pingTimeout: 5000
});

io.on(action.COMMON.CONNECTION, client => {
    const controller = require('./core/controller')(io, client);

    logger.info(`New connection with SocketId = ${client.id} _ Handshake: ${converter.toString(client.handshake.headers)}`);

    /*
    * ============================= REQUEST ================================
    */

    // JOIN_ROOM
    controller.receiver.receiveMsgRequest(action.REQUEST.SERVER.JOIN_ROOM, lstChannelId => processor.joinRoom(controller, client, lstChannelId));
    // CHAT_TO_SOMEONE
    controller.receiver.receiveMsgRequest(action.REQUEST.SERVER.CHAT_TO_SOMEONE, data => processor.chatToSomeOne(controller, client, data));
    // SEND_MESSAGE
    controller.receiver.receiveMsgRequest(action.REQUEST.CHANNEL.SEND_MESSAGE, data => processor.sendMessage(controller, client, data));
    // TYPING
    controller.receiver.receiveMsgRequest(action.REQUEST.CHANNEL.TYPING, data => processor.typing(controller, data));
    // UNTYPING
    controller.receiver.receiveMsgRequest(action.REQUEST.CHANNEL.UNTYPING, data => processor.untyping(controller, data));
    // READ
    controller.receiver.receiveMsgRequest(action.REQUEST.CHANNEL.READ, data => processor.read(controller, client, data));
    // USER_STATUS
    controller.receiver.receiveMsgRequest(action.REQUEST.SERVER.USER_STATUS, data => processor.userStatus(controller, client, data));
    // USER_STATUS
    controller.receiver.receiveMsgRequest(action.REQUEST.SERVER.LIST_USER_STATUS, data => processor.listUserStatus(controller, client, data));

    /*
    * ============================= RESPONSE ================================
    */

    // USER_INFO
    controller.receiver.receiveMsgResponse(action.REQUEST.SERVER.USER_INFO, response => processor.userInfo(controller, client, response));
    // DISCONNECT
    controller.receiver.receiveMsgResponse(action.COMMON.DISCONNECT, reason => processor.disconnect(controller, client, reason));
    // RECONNECT
    controller.receiver.receiveMsgResponse(action.COMMON.RECONNECT, res => {
        logger.info(`${action.COMMON.RECONNECT} _ ${JSON.stringify(res)}`);
    });

    /*
    * ============================= PING / PONG ================================
    */

    client.conn.on('packet', packet => {
        if (packet.type === 'ping') {
            // const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
            // logger.info(`Received ping from client ${userInfo.username} ___ ${JSON.stringify(packet)}`);
        }
    });

    client.conn.on('packetCreate', packet => {
        if (packet.type === 'pong') {
            // const userInfo = handler.getDicUsersOnlineBySocketId(client.id);
            // logger.info(`Sending pong to client ${userInfo.username} ___ ${JSON.stringify(packet)}`);
        }
    });
});