const logger = require('./logger');

module.exports = {
    toString: data => {
        if (typeof data != 'string') {
            return JSON.stringify(data);
        }
        return data;
    },
    toObject: data => {
        if (typeof data == 'string') {
            const convertValue = JSON.parse(data);
            if (!Array.isArray(convertValue)) {
                return convertValue;
            }
            return {};
        }
        if (typeof data == 'object' && !Array.isArray(data)) {
            return data;
        }
        return {};
    },
    toArray: data => {
        if (typeof data == 'string') {
            const convertValue = JSON.parse(data);
            if (Array.isArray(convertValue)) {
                return convertValue;
            }
            return [];
        }
        if (typeof data == 'object' && Array.isArray(data)) {
            return data;
        }
        return [];
    },
    fromArrayToDictionary: (keyInp = '', lstDataInp = []) => {
        const dic = {};
        const key = keyInp;
        try {
            for (let i = 0; i < lstDataInp.length; i++) {
                const data = lstDataInp[i];
                if (typeof data == 'object') {
                    const obj = data;
                    // Kiểm tra xem key truyền vào có trùng tên với thuộc tính của obj thuộc list hay ko.
                    if (!Reflect.has(obj, key)) continue;
                    // Nếu có thì lấy giá trị và kiểm tra xem giá trị đó đã được tạo trong dic chưa.
                    const valueOfKey = obj[key];
                    const keyOfDic = valueOfKey;
                    if (Reflect.has(dic, keyOfDic)) continue;
                    dic[keyOfDic] = obj;
                } else {
                    // Nếu có thì lấy giá trị và kiểm tra xem giá trị đó đã được tạo trong dic chưa.
                    const valueOfKey = data;
                    const keyOfDic = valueOfKey;
                    if (Reflect.has(dic, keyOfDic)) continue;
                    dic[keyOfDic] = data;
                }
            }
        } catch (error) {
            logger.error(`convertArrayToDictionary _ ERR: ${logger.toString(error)} _ INFO: key = ${key}, lstData = ${logger.toString(lstDataInp)}`);
            return {};
        }
        return dic;
    }

};
