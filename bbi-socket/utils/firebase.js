/* eslint-disable no-param-reassign */
const firebase = require('firebase-admin');
const logger = require('./logger');

const serviceAccount = require('../config/bbi-chat-firebase-admin-sak.json');

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: 'https://bbi-chat.firebaseio.com'
});

// {
//     "to" : "<deviceId>",
//         "priority": "high",
//             "content_available": true,
//                 "notification" : {
//         "title" : "<title>",
//             "body"  : "<body>",
//                 "sound" : "default"
//     },
//     "data": {
//         "type": "chat"
//     }
// }

// const payloadDefauult = {
//     notification: {
//         title: 'Notification Title',
//         body: 'Notification Body',
//     }
// };

const optionDefault = {
    priority: 'high',
    timeToLive: 60 * 60 * 24 // 1 day
};

module.exports.sendToDevice = (lstFirebaseToken = [], payload = null, options = null) => {
    if (firebase) {
        if (!options) options = optionDefault;
        logger.info(`List firebase token: ${lstFirebaseToken.toString()}`);
        if (payload && payload.notification && payload.notification.title && payload.notification.body) {
            return firebase.messaging().sendToDevice(lstFirebaseToken, payload, options);
        }
        logger.error('PAYLOAD is Invalid');
    } else {
        logger.error('Firebase NULL');
    }
};
