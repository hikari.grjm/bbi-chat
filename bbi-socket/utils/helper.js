const fs = require('fs');
const path = require('path');
const logger = require('./logger');
const converter = require('./converter');

const parseConfig = (urlCloud, urlDebug) => {
    try {
        const exist = fs.existsSync(urlCloud);
        if (exist) {
            const data = fs.readFileSync(urlCloud, 'utf8');
            if (data) {
                return JSON.parse(data);
            }
        } else {
            const existDebug = fs.existsSync(path.join(urlDebug));
            if (existDebug) {
                const data = fs.readFileSync(path.join(urlDebug), 'utf8');
                if (data) {
                    return JSON.parse(data);
                }
            }
        }
    } catch (error) {
        logger.error(`invalid config: ${error}`);
    }
    return null;
};

module.exports = {
    isNullOrEmpty: data => {
        if (!data) return true;
        let output = data;

        if (typeof output !== 'string') {
            output = output.toString();
        }
        output = output.trim();

        return output.length <= 0;
    },
    removeNewLine: strInput => {
        const strReturn = strInput.replace(/\r|\n|\t/g, ' ');
        return strReturn;
    },
    replaceString: valueInput => {
        return valueInput.replace(/\\/g, '\\\\')
            .replace(/\$/g, '\\$')
            .replace(/'/g, "\\'")
            .replace(/"/g, '\\"');
    },
    replaceKeyDic: valueInput => {
        if (!valueInput) return '';
        if (typeof valueInput != 'string') return valueInput;
        return valueInput.replace(/-/g, '_');
    },
    getNullable: data => {
        if (data === '' || data === false || data === 0) return data;
        if (!data) return null;
        return data;
    },
    getBoolean: data => {
        if (data === '' || data === false || data === 0 || data == '0') return false;
        if (!data) return false;
        return true;
    },
    clone: data => {
        return JSON.parse(converter.toString(data));
    },
    isArrayChanged: (oldArray = [], newArray = []) => {
        return oldArray.every(e => newArray.includes(e));
    },
    isObjectChanged: (oldObj = {}, newObj = {}) => {
        let isSame = true;
        for (const key in newObj) {
            if (!Reflect.has(oldObj, key)) continue;
            if (oldObj[key] !== newObj[key]) {
                isSame = false;
            }
        }
        return isSame;
    },
    isObjectEmpty: obj => {
        for (const key in obj) {
            if (Reflect.has(obj, key)) {
                return false;
            }
        }
        return true;
    },
    getConfig: name => {
        try {
            const pathCloudConfig = `/etc/${name}/config-${name}.json`;
            const pathDebugConfig = `${process.cwd()}/config/config-${name}.json`;
            const content = parseConfig(pathCloudConfig, pathDebugConfig);
            return content;
        } catch (error) {
            logger.error(`getConfig _ ERR: ${error}`);
        }
        return null;
    }
};
