const Axios = require('axios');
const { URL } = require('../common/constant');
const config = require('./helper').getConfig('bbi-socket');
const logger = require('../utils/logger');

let token = config && config.token_admin ? config.token_admin : null;

module.exports.login = (username, password) => {
    return post({
        url: URL.login,
        data: {
            username,
            password
        },
        nonToken: true
    }).then(res => {
        if (!res || !res.token || !res.user_info) return null;
        saveUserInfo(res);
        return res.user_info;
    });
};

module.exports.getListUserStatus = lstUserId => {
    return post({
        url: URL.user_status,
        data: {
            list_user_id: lstUserId
        }
    }).then(res => {
        logger.info(`getListUserStatus: ${JSON.stringify(res)}`);
        if (!res) return [];
        if (res && Reflect.has(res, 'errorCode')) {
            return [];
        }
        return res;
    });
};

module.exports.createMessage = obj => {
    return post({
        url: URL.messages,
        data: obj
    }).then(res => {
        logger.info(`createMessage: ${JSON.stringify(res)}`);
        if (!res) return false;
        if (res && Reflect.has(res, 'errorCode') && res.errorCode != 'SUCCESS') {
            return false;
        }
        return true;
    });
};

module.exports.updateUserStatus = (userId, obj) => {
    // const pathz = `${URL.user_status}/${userId}`;
    // return put({
    //     url: pathz,
    //     data: obj
    // }).then(res => {
    //     if (!res) return false;
    //     if (res && Reflect.has(res, 'errorCode') && res.errorCode != 'SUCCESS') {
    //         return false;
    //     }
    //     return true;
    // });

    // Chuyen sang ko nhan response
    const pathz = `${URL.user_status}/${userId}`;
    return put({
        url: pathz,
        data: obj
    });
};

module.exports.updateMessageStatus = (messageId, obj) => {
    const pathz = `${URL.messages}/${messageId}`;
    return put({
        url: pathz,
        data: obj
    }).then(res => {
        if (!res) return false;
        if (res && Reflect.has(res, 'errorCode') && res.errorCode != 'SUCCESS') {
            return true;
        }
        return false;
    });
};

function saveUserInfo(resLogin) {
    token = resLogin.token;
    renewAccessToken(8);
}

function renewAccessToken(time) {
    setTimeout(() => {
        get({
            url: URL.refresh
        }).then(res => {
            if (!res) {
                renewAccessToken(time);
            } else {
                token = res.token;
                renewAccessToken(time);
            }
        });
    }, time * 60 * 1000 * 0.75);
}

function get({ url, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: `bearer ${token}`
                }
        };

        Axios.get(
            url, config
        )
            .then((response = {}) => {
                resolve(response.data);
            })
            .catch(error => {
                logger.error(`Get ${url} fail: ${error}`);
                resolve(null);
            });
    });
}

function post({ url, data, nonToken = false, contentType, onUploadProgress }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: `bearer ${token}`
                }
        };
        if (contentType) {
            if (!config.headers) config.headers = {};
            config.headers['Content-Type'] = contentType;
        }
        if (onUploadProgress) {
            config.onUploadProgress = onUploadProgress;
        }

        Axios.post(
            url, data, config
        )
            .then((response = {}) => {
                resolve(response.data);
            })
            .catch(error => {
                logger.error(`Post ${url} fail: ${error}`);
                resolve(null);
            });
    });
}

function put({ url, data, nonToken = false }) {
    return new Promise(resolve => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: `bearer ${token}`
                }
        };

        Axios.put(
            url, data, config
        )
            .then((response = {}) => {
                resolve(response.data);
            })
            .catch(error => {
                logger.error(`Put ${url} fail: ${error}`);
                resolve(null);
            });
    });
}

function putWithoutResponse({ url, data, nonToken = false }) {
    return new Promise(() => {
        const config = {
            headers: nonToken
                ? null
                : {
                    Authorization: `bearer ${token}`
                }
        };

        Axios.put(url, data, config);
    });
}

// function del({ url, nonToken = false }) {
//     return new Promise(resolve => {
//         const config = {
//             headers: nonToken
//                 ? null
//                 : {
//                     Authorization: `bearer ${token}`
//                 }
//         };

//         Axios.delete(
//             url, config
//         )
//             .then((response = {}) => {
//                 resolve(response.data);
//             })
//             .catch(error => {
//                 logger.error(`Delete ${url} fail: ${error}`);
//                 resolve(null);
//             });
//     });
// }