/* eslint-disable no-console */
const request = require('request');
const { getConfig } = require('./helper');
const configTelegram = getConfig('logger').telegram;
const chatId = configTelegram.chat_id;
const serverUrl = configTelegram.uri;

const sendToTelegram = msg => {
    if (!serverUrl || !chatId || !configTelegram.enable) {
        return;
    }
    const options = {
        uri: serverUrl,
        method: 'POST',
        json: {
            chat_id: chatId,
            text: msg
        }
    };
    request(options);
    return;
};

module.exports = {
    info: msg => {
        return msg;
        // sendToTelegram(`SOCKET INFO: ${msg}`);
        // return console.log(msg);
    },
    error: msg => {
        sendToTelegram(`SOCKET ERROR: ${msg}`);
        return console.error(msg);
    },
    sendToTelegram
};